<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('index');
Route::get('/kontakt', 'HomeController@contact')->name('contact');
Route::get('/logowanie', 'HomeController@login')->name('logowanie');
Route::get('/profil/{id}', 'HomeController@profile')->name('profile');
Route::get('/edytuj/{id}', 'HomeController@edit')->name('edytuj');
Route::get('/addtofav/{id}', 'HomeController@fav')->name('fav');
Route::get('/delfav/{id}', 'HomeController@delfav')->name('delfav');
Route::get('/delcat/{id}', 'HomeController@delcat')->name('delcat');
Route::post('/edit_cats', 'HomeController@edit_cats')->name('edit_cats');
Route::post('/edit_desc', 'HomeController@edit_desc')->name('edit_desc');
Route::post('/edit_details', 'HomeController@edit_details')->name('edit_details');
Route::get('/wyloguj', 'HomeController@logout')->name('logout');
Route::get('/rejestracja', 'HomeController@register')->name('rejestracja');
Route::get('/fachowcy', 'HomeController@agents')->name('agents');
Route::get('/wyszukiwarka/{region}/{city}/{refval}/{category}', 'HomeController@search')->name('search');
Route::post('/search_form', 'HomeController@search_form')->name('search_form');
Route::post('/api/email_poster','HomeController@email_poster')->name('email_poster');
Route::post('/change_av', 'HomeController@change_av')->name('change_av');
Route::post('/crop_av', 'HomeController@crop_av')->name('crop_av');
Route::post('/gallery_add', 'HomeController@gallery_add')->name('gallery_add');
Route::get('/deletephoto/{id}', 'HomeController@deletephoto')->name('deletephoto');
Route::post('/addopinion', 'HomeController@addopinion')->name('addopinion');
Route::post('/registerpost', 'HomeController@registerpost')->name('registerpost');
Route::post('/contactsend', 'HomeController@contactsend')->name('contactsend');
Route::get('/getstate/{name}', 'HomeController@getstate')->name('getstate');
Route::get('/getcity/{name}', 'HomeController@getcity')->name('getcity');
Route::get('/getcat/{name}', 'HomeController@getcat')->name('getcat');

Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::get('/updateusersavg', function () {
    \Illuminate\Support\Facades\Artisan::call('update:avg');
});
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Auth::routes();
