@extends('welcome')

@section('bdcl') full-screen login @stop
@section('nvcl') navbar navbar-expand-lg fixed-top nav-down navbar-transparent stay-trans @stop
@section('seo')
    <title>Twoja Ekipa - Logowanie</title>
    <meta name="description" content="Serwis Twoja Ekipa przynosi szeroki wybór fachowców, stale aktualizowany indeks firm oraz trendy rynkowe. Jeżeli poszukujesz fachowej ekipy to jesteś w dobrych rękach." />
    <meta property="og:title" content="Twoja Ekipa - Jeżeli poszukujesz fachowej ekipy to jesteś w dobrych rękach."/>
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="https://twoja-ekipa.pl/logowanie"/>
    <meta property="og:image" content="https://twoja-ekipa.pl/images/fb.jpg"/>
    <meta property="og:description" content="Serwis Twoja Ekipa przynosi szeroki wybór fachowców, stale aktualizowany indeks firm oraz trendy rynkowe."/>
@stop
@section('content')
    <div class="wrapper">
        <div class="page-header" style="background-image: url('../assets/img/sections/bruno-abatti.jpg');">
            <div class="filter"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-xs-12 ml-auto mr-auto">
                        <div class="card card-register card-login-now" style="    min-height: 320px;">
                            <h3 style="margin-bottom: 0;" class="card-title col-xs-12 text-left"><b><span class="text-danger text-uppercase">Zresetuj hasło</span></b></h3>
                            <form class="form-horizontal" method="POST" action="/password/reset">
                                {{ csrf_field() }}

                                <input type="hidden" name="token" value="{{ $token }}">

                                <br>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Adres email" required autofocus>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <input id="password" type="password" class="form-control" name="password"
                                           value="{{ old('email') }}" placeholder="Hasło" required autofocus>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-block btn-lg">
                                        Resetuj hasło
                                    </button>
                                </div>

                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@stop