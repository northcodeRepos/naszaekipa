@extends('welcome')

@section('bdcl') profile @stop
@section('nvcl') navbar navbar-expand-lg super-nav fixed-top nav-down navbar-transparent @stop
@section('content')
    <div class="wrapper">
        <div class="page-header page-header-small"
             style="background-image: url('../assets/img/sections/rodrigo-ardilha.jpg');">
            <div class="filter"></div>
        </div>
        <div class="profile-content section">
            <div class="container">
                <div class="row">
                    <div class="profile-picture text-center">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            @include('partials.user.edit_avatar', ['user' => $user])
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="edit-panel col-md-12 ml-auto mr-auto text-center">
                        <div class="text-center col-md-12 ml-auto mr-auto">
                            @include('partials.user.edit_crop')
                        </div>
                    </div>
                    <div class="edit-panel col-md-12 ml-auto mr-auto text-center">
                        <div class="text-left col-md-12 ml-auto mr-auto">
                            <div class="name">
                                <h4 class="title text-center" style="margin-bottom: 10px;">{{$user->user_nicename}}<br>
                                    <small>@if($user->user_isperformer == 1) WYKONAWCA @else KLIENT @endif</small>
                                </h4>
                            </div>
                            <hr>
                        </div>

                        @include('partials.user.edit_categories', ['user' => $user])
                        @include('partials.user.edit_details', ['user' => $user])
                        @include('partials.user.edit_desc')
                        @include('partials.user.edit_gallery', ['user' => $user])

                    </div>

                </div>

            </div>

        </div>

    </div>
@stop

@section('page_scripts')
    @if(Auth::check())
        <script>
            var button_wait = 'Proszę czekać';
            function handleAjaxPost(button, route, data) {
                var old = button.html();
                button.html(button_wait);
                $.ajax({
                    type: 'post',
                    url: route,
                    dataType: 'json',
                    data: data,
                    success: function (data) {
                        handleAjaxData(data);
                        button.html(old);
                    }
                });
            }
            function submitAjaxRequest(e) {
                var formData = new FormData(this);
                $.ajax({
                    type: 'POST',
                    url: "{{route('change_av')}}",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        var data = JSON.parse(data);
                        if (data.error) {
                            error(data);
                        } else {
                            $('.crop_av img').attr('src', '/avatars/' + data.src);
                            var croper = $('.crop_av img').cropper({
                                aspectRatio: 1 / 1,
                                crop: function (e) {
                                    var data = croper.cropper('getData');
                                    $('input[name="x"]').val(data['x']);
                                    $('input[name="y"]').val(data['y']);
                                    $('input[name="xx"]').val(data['width']);
                                    $('input[name="yy"]').val(data['height']);
                                }
                            }).on('cropend', function (e) {
                                var data = croper.cropper('getData');
                                $('input[name="x"]').val(data['x']);
                                $('input[name="y"]').val(data['y']);
                                $('input[name="xx"]').val(data['width']);
                                $('input[name="yy"]').val(data['height']);
                                // Prevent to start cropping, moving, etc if necessary
                                if (e.action === 'crop') {
                                    e.preventDefault();
                                }
                            });
                            $('.fileinput-new').hide();
                            $('.crop_av').show();
                        }
                    }
                });
                return false;
            };
            function submitCropRequest(e) {
                var formData = new FormData(this);
                $.ajax({
                    type: 'POST',
                    url: "{{route('crop_av')}}",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (data) {
                        data = JSON.parse(data);
                        if (data.error) {
                            error(data);
                        } else {
                            window.location.reload();
                        }
                    }
                });
                return false;
            };

            $(document).on('click', '.remove_cat', function () {
                $(this).closest('.position-relative').remove();
            });

            $('.addcat').on('click', function () {
                var newCat = '';
                newCat += '<div class="col-md-6 col-xs-12 position-relative mt-2">';
                newCat += '<div class="row">';
                newCat += '<div class="col-md-1 col-1 d-flex p-0 justify-content-center align-items-center"><i class="fa fa-id-card"></i></div>';
                newCat += '<div class="col-md-11 col-11 pl-0">';
                newCat += '<ul class="hidden_drop dropdown-menu dropdown-info"></ul>';
                newCat += '<input type="text" autocomplete="off" name="category' + ($('#cats .row input').length + 1) + '" placeholder="Kategoria ' + ($('#cats .row input').length + 1) + '" class="form-control drop-inpt mb-0" required>';
                newCat += '<span class="remove_cat btn btn-sm btn-danger"><i class="fa fa-trash"></i> Usuń</span>';
                newCat += '</div></div></div>';
                $('#cats').append(newCat);
            });
            $(document).on('change', 'input[name=avatar]', function () {
                $(this).closest('form').submit();
            });
            $(document).on('submit', '.fileinput-new', submitAjaxRequest);
            $(document).on('submit', '.crop_av', submitCropRequest);
            $(document).on('submit', '.edit_form', function () {
                handleAjaxPost($(this).find('button[type=submit]'), $(this).attr('action'), $(this).serialize());
                return false;
            });
        </script>
    @endif


@stop