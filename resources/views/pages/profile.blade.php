@extends('welcome')

@section('bdcl') profile @stop
@section('nvcl') navbar navbar-expand-lg super-nav fixed-top nav-down navbar-transparent @stop
@section('seo')
    <title>Twoja Ekipa - {{$user->user_nicename}}
        - {{$user->company_name}} @if($user->categories)<?php $c = 1; ?>@foreach($user->categories as $cat)@if($c <= 5)
            - <?php echo $cat->name?> @endif<?php $c++; ?> @endforeach @endif</title>
    <meta name="description" content="{{$user->user_desc}}"/>

    @if($user->company_name)
        <meta property="og:title" content="{{$user->company_name}}"/>
    @else
        <meta property="og:title" content="{{$user->user_nicename}}"/>
    @endif
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="https://twoja-ekipa.pl/profil/{{$user->id}}"/>
    <meta property="og:image"
          content="@if($user->getFirstMediaUrl('avatars')) https://twoja-ekipa.pl{{$user->getFirstMediaUrl('avatars')}} @else ttps://twoja-ekipa.pl/avatars/default-avatar.png  @endif"/>
    @if($user->user_desc)
        <meta property="og:description" content="{{$user->user_desc}}"/>
    @else
        <meta property="og:description" content="Zapraszam na mój profil w serwisie Twoja-Ekipa."/>
    @endif
@stop
@section('content')
    <div class="wrapper">
        <div class="page-header page-header-small"
             style="background-image: url('../assets/img/sections/rodrigo-ardilha.jpg');">
            <div class="filter"></div>
        </div>
        <div class="profile-content section">
            <div class="container">
                <div class="row">
                    <div class="profile-picture text-center">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new img-no-padding">
                                <img src="@if($user->getFirstMediaUrl('avatars')) {{$user->getFirstMediaUrl('avatars')}} @else /avatars/default-avatar.png  @endif"
                                     alt="...">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 ml-auto mr-auto text-center">
                        <div class="text-left col-md-12 ml-auto mr-auto">
                            @if($user->company_name && $user->user_isperformer)
                                <div class="name">
                                    <h4 class="title text-center font-weight-bold"
                                        style="margin-bottom: 0px;">{{$user->company_name}}</h4>
                                </div>
                            @endif
                            <div class="name">
                                <h4 class="title text-center"
                                    style="margin-bottom: 10px;margin-top: 0;">{{$user->user_nicename}}<br>
                                    <small>@if($user->user_isperformer == 1) WYKONAWCA @else KLIENT @endif</small>
                                </h4>
                            </div>
                            @if($user->categories)
                                <p style="    font-size: 18px;font-weight: 500;"
                                   class="text-danger text-center category">
                                    <?php $c = 1; ?>
                                    @foreach($user->categories as $cat)
                                        @if($c <= 5)
                                            @if($c == 3) <br> @endif
                                            @<?php echo $cat->name?>
                                        @endif
                                        <?php $c++; ?>
                                    @endforeach
                                </p>
                            @endif
                            <hr>
                        </div>
                        @if(Auth::check())

                            @if($user->followers->where('user_id', Auth::user()->id)->first())

                                <a href="{{route('delfav', $user->id)}}"
                                   class="btn btn-outline-danger btn-round"><i
                                            class="fa fa-star"></i> Usuń z ulubionych</a>

                            @elseif(!$user->followers->where('user_id', Auth::user()->id)->first() && $user->id != Auth::user()->id)

                                <a href="{{route('fav', $user->id)}}"
                                   class="btn btn-outline-default btn-round"><i
                                            class="fa fa-star"></i> Dodaj do ulubionych</a>

                            @else

                                <a href="{{route('edytuj', $user->id)}}"
                                   class="btn btn-outline-default btn-round"><i
                                            class="fa fa-cog"></i> Edytuj profil</a>


                            @endif
                        @endif
                        @if($user->user_isperformer)
                            <a href="https://www.facebook.com/sharer/sharer.php?u=https://twoja-ekipa.pl/profil/{{$user->id}}"
                               class="btn btn-info btn-round" target="_blank">Udostępnij na <i
                                        class="fa fa-facebook"></i></a>
                            <a style="background: #4d9ed8;border-color:#4d9ed8;"
                               href="https://twitter.com/home?status=https://twoja-ekipa.pl/profil/{{$user->id}}"
                               class="btn btn-info btn-round" target="_blank">Udostępnij na <i
                                        class="fa fa-twitter"></i></a>

                        @endif
                        <hr>
                        @if($user->user_desc)
                            <div class="text-left col-md-12 ml-auto mr-auto">
                                {{$user->user_desc}}
                                <hr>
                            </div>
                        @endif
                        <div class="text-left col-xs-12"
                             style="display: flex;flex-wrap: wrap;    justify-content: flex-start;">

                            @if($user->email)
                                <h5 class="col-md-6 col-xs-12" style="margin-top: 3px;">
                                    <b>
                                        <small><i class="fa fa-envelope"></i></small>
                                        Email:</b> <a href="mailto:{{$user->email}}">{{$user->email}}</a>
                                </h5>
                            @endif
                            @if($user->user_city && $user->user_zipcode)
                                <h5 class="col-md-6 col-xs-12" style="margin-top: 3px;">
                                    <b>
                                        <small><i class="fa fa-globe"></i></small>
                                        Miasto:</b>
                                    <span>{{$user->user_city}}
                                        <small>{{$user->user_zipcode}}</small> </span>
                                </h5>
                            @endif
                            @if($user->user_country)
                                <h5 class="col-md-6 col-xs-12" style="margin-top: 3px;">
                                    <b>
                                        <small><i class="fa fa-globe"></i></small>
                                        Państwo:</b> <span>{{$user->user_country}}</span>
                                </h5>
                            @endif
                            @if($user->user_address)
                                <h5 class="col-md-6 col-xs-12" style="margin-top: 3px;"><b>
                                        <small><i class="fa fa-home"></i></small>
                                        Adres:</b> <span>{{$user->user_address}}</span>
                                </h5>
                            @endif
                            @if($user->user_number)
                                <h5 class="col-md-6 col-xs-12" style="margin-top: 3px;"><b>
                                        <small><i class="fa fa-phone"></i></small>
                                        Numer:</b> <span
                                            class="text-success">{{$user->user_number}}</span>
                                </h5>
                            @endif
                            @if($user->user_regon)
                                <h5 class="col-md-6 col-xs-12" style="margin-top: 3px;"><b>
                                        <small><i class="fa fa-hashtag"></i></small>
                                        Regon:</b>
                                    <span>{{$user->user_regon}}</span>
                                </h5>
                            @endif
                            @if($user->user_nip)
                                <h5 class="col-md-6 col-xs-12" style="margin-top: 3px;"><b>
                                        <small><i class="fa fa-hashtag"></i></small>
                                        Nip:</b>
                                    <span>{{$user->user_nip}}</span>
                                </h5>
                            @endif
                            @if($user->user_url)
                                <h5 class="col-md-6 col-xs-12" style="margin-top: 3px;">
                                    <b>
                                        <small><i class="fa fa-external-link"></i></small>
                                    </b>
                                    <b>www:</b>
                                    <a href="http://{{str_replace("https://","",str_replace("http://","",$user->user_url))}}"
                                       target="_blank">{{str_replace("https://","",str_replace("http://","",$user->user_url))}}</a>
                                </h5>
                            @endif
                            @if($user->user_fb)
                                <h5 class="col-md-6 col-xs-12" style="margin-top: 3px;">
                                    <b>
                                        <small><i class="fa fa-facebook"></i></small>
                                    </b>
                                    <b>Facebook:</b>
                                    <a href="http://{{str_replace("https://","",str_replace("http://","",$user->user_fb))}}"
                                       target="_blank">{{str_replace("https://","",$user->user_fb)}}</a>
                                </h5>
                            @endif
                            @if($user->user_tw)
                                <h5 class="col-md-6 col-xs-12" style="margin-top: 3px;">
                                    <b>
                                        <small><i class="fa fa-twitter"></i></small>
                                    </b>
                                    <b>Twitter:</b>
                                    <a href="http://{{str_replace("https://","",str_replace("http://","",$user->user_tw))}}"
                                       target="_blank">{{str_replace("https://","",$user->user_tw)}}</a>
                                </h5>
                            @endif
                        </div>
                        @if($user->user_isperformer == 1)

                            @if($user->getMedia('portfolio')->count() > 1)
                                <div class="col-md-12 col-xs-12">&nbsp;</div>
                                <div class="col-md-12 col-xs-12">&nbsp;</div>
                                <div class="col-md-12"><h4 class="text-left">Galeria prac użytkownika :</h4><br></div>
                                <div class="col-md-12">
                                    <div id="gallery" class="col-md-12" style="padding: 0;">
                                        @foreach($user->getMedia('portfolio')->reverse() as $media)
                                            <div class="card grid-item "
                                                 style="overflow:hidden;padding: 0;width: 31%;margin: 1%;">
                                                <a href="{{$media->getUrl()}}" data-lightbox="portfolio"
                                                   data-title=""><img
                                                            src="{{$media->getUrl()}}" alt=""></a>
                                            </div>
                                        @endforeach
                                    </div>

                                </div>
                            @endif
                            <div class="col-md-12 col-xs-12">&nbsp;</div>
                            <div class="col-md-12 col-xs-12">&nbsp;</div>
                        @endif
                    </div>
                </div>

            </div>
            <div class="container">
                <br>

                <div class="nav-tabs-navigation">
                    <div class="nav-tabs-wrapper">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#follows" role="tab">Dodali mnie do
                                    ulubionych</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#following" role="tab">Dodałem do
                                    ulubionych</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="follows" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12 ml-auto mr-auto">
                                <ul class="list-unstyled follows" style="    display: flex;
    flex-wrap: wrap;">
                                    @foreach($user->followers as $follwer)
                                        <?php $userData = \App\User::find($follwer->user_id); ?>
                                        <li style="display: flex;width: 33.3%;">
                                            <a href="{{route('profile', $follwer->user_id)}}" target="_blank"
                                               style="width: 100%;" class="row">
                                                <div class="col-md-2 text-center col-xs-12">
                                                    <img width="40"
                                                         src="@if($userData->getFirstMediaUrl('avatars')) {{$userData->getFirstMediaUrl('avatars')}} @else /avatars/default-avatar.png  @endif"
                                                         alt="Circle Image"
                                                         class="img-circle img-no-padding img-responsive">
                                                </div>
                                                <div class="col-md-10 col-xs-12">
                                                    <h6>{{$follwer->user_nicename}}<br>
                                                        <small>@if($follwer->user_isperformer == 1) WYKONAWCA @else
                                                                KLIENT @endif</small>
                                                    </h6>
                                                </div>
                                            </a>
                                        </li>
                                    @endforeach

                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane " id="following" role="tabpanel">
                        <div class="row">
                            <div class="col-md-12 ml-auto mr-auto">
                                <ul class="list-unstyled follows" style="    display: flex;
    flex-wrap: wrap;">
                                    @foreach($user->follows as $follower)
                                        <?php $userData = \App\User::where('id', $follower->fav_id)->first(); ?>

                                        <li style="display: flex;width: 33.3%;">
                                            <a href="{{route('profile', $follower->fav_id)}}" target="_blank"
                                               style="width: 100%;" class="row">
                                                <div class="col-md-2 text-center col-xs-12">

                                                    <img width="40"
                                                         src="@if($userData->getFirstMediaUrl('avatars')) {{$userData->getFirstMediaUrl('avatars')}} @else /avatars/default-avatar.png  @endif"
                                                         alt="Circle Image"
                                                         class="img-circle img-no-padding img-responsive">
                                                </div>
                                                <div class="col-md-10 col-xs-12">
                                                    <h6>{{$follower->user_nicename}}<br>
                                                        <small>@if($follower->user_isperformer == 1) WYKONAWCA @else
                                                                KLIENT @endif</small>
                                                    </h6>
                                                </div>
                                            </a>
                                        </li>
                                    @endforeach

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="faq">
                    @if($user->opinions->where('op_verified',1)->count() > 0)
                        <h4>Opinie na temat użytkownika:</h4> <br>
                        <div id="acordeon">
                            <div id="accordion" role="tablist" aria-multiselectable="true">
                                <?php $h = 1; ?>
                                @foreach($user->opinions->where('op_verified', 1) as $op)

                                    <div class="card no-transition">
                                        <div class="card-header card-collapse" role="tab" id="heading{{$h}}">
                                            <h5 class="mb-0 panel-title">
                                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion"
                                                   href="#collapse{{$h}}" aria-expanded="false"
                                                   aria-controls="collapse{{$h}}">
                                            <span><b>Użytkownik <b>{{(\App\User::find($op->op_id)->first())->user_nicename}}</b> ocenił na
                                                    <?php
                                                    $i = 1;
                                                    while ($i <= round($op->op_rate)):
                                                    ?> <span class="fa fa-star"></span> <?php
                                                    $i++;
                                                    endwhile;
                                                    ?>

                                                </b></span>
                                                    <span class="pull-right"> <i
                                                                class="nc-icon nc-minimal-down"></i></span>
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse{{$h}}" class="collapse" role="tabpanel"
                                             aria-labelledby="heading{{$h}}"
                                             style="">


                                            <div class="card-body">
                                                <h5>Data wykonania usługi: <small><b>{{\Carbon\Carbon::parse($op->service_date)}}</b></small></h5>
                                                <h5>Profesjonalizm: <small><b>@for ($i = 0; $i < $op->initial_impression; $i++) <i class="fa fa-star"></i> @endfor</b></small></h5>
                                                <h5>Terminowość: <small><b>@for ($i = 0; $i < $op->punctuality; $i++) <i class="fa fa-star"></i> @endfor</b></small></h5>
                                                <h5>Jakość: <small><b>@for ($i = 0; $i < $op->quality; $i++) <i class="fa fa-star"></i> @endfor</b></small></h5>
                                                <h5>Cena: <small><b>@for ($i = 0; $i < $op->price; $i++) <i class="fa fa-star"></i> @endfor</b></small></h5>
                                                <h5>Ogółem: <small><b>@for ($i = 0; $i < $op->overall; $i++) <i class="fa fa-star"></i> @endfor</b></small></h5>
                                                <hr>
                                                    <h5>Czy zarekomendowałbyś/łabyś tego wykonawcę znajomym?
                                                        <small><b>@if($op->would_recommend) Tak @else Nie @endif</b></small></h5>
                                                <hr>
                                                <h5>Komentarz: <small><b>{{$op->message}}</b></small></h5>
                                                @if($op->getMedia('opinion')->count() > 0)

                                                    <hr>
                                                    <h5>Dołączone zdjęcia</h5>
                                                <div class="col-md-12 pl-0 pr-0 opinion_gallery">
                                                        @foreach($op->getMedia('opinion')    as $media)
                                                            <div class="card grid-item d-inline-block "
                                                                 style="overflow:hidden;padding: 0;width: 31%;margin: 1%;">
                                                            <a href="{{$media->getUrl()}}" data-lightbox="opinion_{{$op->id}}"
                                                               data-title=""><img
                                                                        src="{{$media->getUrl()}}" alt=""></a>
                                                            </div>
                                                        @endforeach
                                                </div>
                                                @endif
                                                <br>
                                                <br><br><b>Użytkownik
                                                    <a style="font-weight: 800;"
                                                       href="{{route('profile', $op->user_id)}}"
                                                       target="_blank">{{(\App\User::find($op->op_id)->first())->user_nicename}}</a>
                                                    ocenił na
                                                    <?php
                                                    $i = 1;
                                                    while ($i <= round($op->op_rate)):
                                                    ?> <span class="fa fa-star"></span> <?php
                                                    $i++;
                                                    endwhile;
                                                    ?></b>
                                            </div>
                                        </div>

                                    </div>
                                    <?php $h++; ?>
                                @endforeach
                            </div><!--  end acordeon -->
                        </div>
                    @else
                        <h4>Opinie na temat użytkownika :</h4> <br>
                        <p>Brak opinii na temat użytkownika.</p>
                        <br><br><br>
                    @endif
                    @if(Auth::check() && Auth::user()->id != $user->id)
                        @if($user->opinions->where('user_id', Auth::user()->id)->where('op_id', $user->id)->where('op_verified', 1)->first())
                            <br>
                            <p>Dziękujemy za dodanie opinii.</p>
                        @elseif($user->opinions->where('user_id', Auth::user()->id)->where('op_id', $user->id)->where('op_verified', 0)->first())
                            <br>
                            <h4 class="text-info">Twoja opinia oczekuje na weryfikacje przez moderatora.</h4>
                        @elseif(!$user->opinions->where('user_id', Auth::user()->id)->where('op_id', $user->id)->first())
                            <div class="row">
                                <form class="col-md-12 add_opinion" enctype="multipart/form-data" method="POST"
                                      id="add_opinion" action="{{route('addopinion')}}">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4 class="mb-4"><b>Dodaj opinię o użytkowniku {{$user->user_nicename}}</b>
                                            </h4>
                                        </div>

                                        <fieldset class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label  class="col-md-12 control-label">Data wykonania usługi:</label>
                                                <div class='input-group date' id='service_date'>
                                                    <input type='text' name="service_date"
                                                           class="form-control datetimepicker"
                                                           placeholder="Luty" required/>
                                                    <span class="input-group-addon">
                                                   <span class="glyphicon glyphicon-calendar"><i class="fa fa-calendar"
                                                                                                 aria-hidden="true"></i></span>
                                               </span>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="col-md-6 col-sm-12">

                                        </fieldset>
                                        <fieldset class="col-md-6 col-lg-4 col-sm-12">
                                            <div class="form-group stars_check">
                                                <label class="col-md-12 control-label" >Profesjonalizm:</label>
                                                    <div class="card-stars">
                                                        <i class="fa fa-star active" data-id="1" aria-hidden="true"></i>
                                                        <i class="fa fa-star" data-id="2" aria-hidden="true"></i>
                                                        <i class="fa fa-star" data-id="3" aria-hidden="true"></i>
                                                        <i class="fa fa-star" data-id="4" aria-hidden="true"></i>
                                                        <i class="fa fa-star" data-id="5" aria-hidden="true"></i>
                                                    </div>
                                                    <input style="opacity: 0;position: absolute;" type="number"
                                                           name="initial_impression" min="1" max="5" value="1" required>
                                            </div>
                                        </fieldset>
                                        <fieldset class="col-md-6 col-lg-4 col-sm-12">
                                            <div class="form-group stars_check">
                                                <label  class="col-md-12 control-label">Terminowość:</label>
                                                    <div class="card-stars">
                                                        <i class="fa fa-star active" data-id="1" aria-hidden="true"></i>
                                                        <i class="fa fa-star" data-id="2" aria-hidden="true"></i>
                                                        <i class="fa fa-star" data-id="3" aria-hidden="true"></i>
                                                        <i class="fa fa-star" data-id="4" aria-hidden="true"></i>
                                                        <i class="fa fa-star" data-id="5" aria-hidden="true"></i>
                                                    </div>
                                                    <input style="opacity: 0;position: absolute;" type="number"
                                                           name="punctuality" min="1" max="5" value="1" required>
                                            </div>
                                        </fieldset>

                                        <fieldset class="col-md-6 col-lg-4 col-sm-12">
                                            <div class="form-group stars_check">
                                                <label  class="col-md-12 control-label">Jakość:</label>
                                                    <div class="card-stars">
                                                        <i class="fa fa-star active" data-id="1" aria-hidden="true"></i>
                                                        <i class="fa fa-star" data-id="2" aria-hidden="true"></i>
                                                        <i class="fa fa-star" data-id="3" aria-hidden="true"></i>
                                                        <i class="fa fa-star" data-id="4" aria-hidden="true"></i>
                                                        <i class="fa fa-star" data-id="5" aria-hidden="true"></i>
                                                    </div>
                                                    <input style="opacity: 0;position: absolute;" type="number"
                                                           name="quality" min="1" max="5" value="1" required>
                                            </div>
                                        </fieldset>
                                        <fieldset class="col-md-6 col-lg-4 col-sm-12">
                                            <div class="form-group stars_check">
                                                <label  class="col-md-12 control-label">Cena:</label>
                                                    <div class="card-stars">
                                                        <i class="fa fa-star active" data-id="1" aria-hidden="true"></i>
                                                        <i class="fa fa-star" data-id="2" aria-hidden="true"></i>
                                                        <i class="fa fa-star" data-id="3" aria-hidden="true"></i>
                                                        <i class="fa fa-star" data-id="4" aria-hidden="true"></i>
                                                        <i class="fa fa-star" data-id="5" aria-hidden="true"></i>
                                                    </div>
                                                    <input style="opacity: 0;position: absolute;" type="number"
                                                           name="price" min="1" max="5" value="1" required>
                                            </div>
                                        </fieldset>
                                        <fieldset class="col-md-6 col-lg-4 col-sm-12">
                                            <div class="form-group stars_check">
                                                <label  class="col-md-12 control-label">Ogółem:</label>
                                                    <div class="card-stars">
                                                        <i class="fa fa-star active" data-id="1" aria-hidden="true"></i>
                                                        <i class="fa fa-star" data-id="2" aria-hidden="true"></i>
                                                        <i class="fa fa-star" data-id="3" aria-hidden="true"></i>
                                                        <i class="fa fa-star" data-id="4" aria-hidden="true"></i>
                                                        <i class="fa fa-star" data-id="5" aria-hidden="true"></i>
                                                    </div>
                                                    <input style="opacity: 0;position: absolute;" type="number"
                                                           name="overall" min="1" max="5" value="1" required>
                                            </div>
                                        </fieldset>
                                        <fieldset class="col-md-12 col-lg-12 col-sm-12">
                                            <div class="form-group">
                                                <label  class="col-md-12 control-label">Czy zarekomendowałbyś/łabyś tego wykonawcę znajomym?</label>
                                                <div class="form-check-radio d-inline-block">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="radio"
                                                               name="would_recommend" id="would_recommend1" value="1"
                                                               checked>
                                                        Tak
                                                        <span class="form-check-sign"></span>
                                                    </label>
                                                </div>
                                                <div class="form-check-radio d-inline-block">
                                                    <label class="form-check-label">
                                                        <input class="form-check-input" type="radio"
                                                               name="would_recommend" id="would_recommend2"
                                                               value="0">
                                                        Nie
                                                        <span class="form-check-sign"></span>
                                                    </label>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <fieldset class="col-md-12 col-sm-12">
                                            <div class="form-group">
                                                <label  class="col-md-12 control-label">Komentarz</label>
                                                <textarea class="form-control" min="50" max="1000" name="message"
                                                          placeholder="Komentarz:"
                                                          rows="7" required></textarea>
                                                <label class="text-right text-info" style="width: 100%;">
                                                    <small>Pozostalo 1000/1000 znaków.</small>
                                                </label>
                                            </div>
                                        </fieldset>
                                        <fieldset class="col-md-12 col-sm-12">
                                            <div class="form-group">
                                                <label  class="col-md-12 control-label">Jeśli chcesz pochwalić się pracą wykonaną przez tego wykonawcę,
                                                    prześlij jej zdjęcia tutaj. Wystarczy wybrać Wybierz plik poniżej,
                                                    aby dodać zdjęcie. <br><span class="text-info">Maksymalny rozmiar pliku to <b>1mb</b>.<br>Akceptujemy : <b>jpg, gif, png</b></span></label>
                                            </div>
                                        </fieldset>
                                        <fieldset class="col-md-12 col-sm-12">
                                            <div class="form-group">
                                                <label class="col-md-12 control-label">Pierwsze zdjęcie</label>
                                                <input type="file" name="media1" max="1000" class="form-control">
                                            </div>
                                        </fieldset>
                                        <fieldset class="col-md-12 col-sm-12">
                                            <div class="form-group">
                                                <label class="col-md-12 control-label">Drugie zdjęcie</label>
                                                <input type="file" name="media2" max="1000" class="form-control">
                                            </div>
                                        </fieldset>
                                        <fieldset class="col-md-12 col-sm-12">
                                            <div class="form-group">
                                                <label class="col-md-12 control-label">Trzecie zdjęcie</label>
                                                <input type="file" name="media3" max="1000" class="form-control">
                                            </div>
                                        </fieldset>
                                        <fieldset class="col-md-12 col-sm-12">
                                            <div class="form-group">
                                                <button disabled type="submit" class="btn btn-info btn-lg">Wyślij ocenę
                                                    użytkownika
                                                </button>
                                            </div>
                                        </fieldset>
                                    </div>
                                </form>
                            </div>
                        @endif
                    @endif
                    @if(!Auth::check())
                        <br><br><br><br>
                        <a href="{{route('rejestracja')}}"
                           class="btn btn-outline-default btn-round"><i
                                    class="fa fa-star"></i> Dodaj opinię</a>
                    @endif
                </div>
            </div>
        </div>

    </div>
@stop


@section('page_scripts')
    <script>
        $(function () {

        })
    </script>
@stop