@extends('welcome')

@section('nvcl') navbar navbar-expand-lg super-nav fixed-top nav-down navbar-transparent @stop
@section('seo')
    <title>Twoja Ekipa - Strona Główna</title>
    <meta name="description" content="Serwis Twoja Ekipa przynosi szeroki wybór fachowców, stale aktualizowany indeks firm oraz trendy rynkowe. Jeżeli poszukujesz fachowej ekipy to jesteś w dobrych rękach." />
    <meta property="og:title" content="Twoja Ekipa - Jeżeli poszukujesz fachowej ekipy to jesteś w dobrych rękach."/>
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="https://twoja-ekipa.pl/"/>
    <meta property="og:image" content="https://twoja-ekipa.pl/images/fb.jpg"/>
    <meta property="og:description" content="Serwis Twoja Ekipa przynosi szeroki wybór fachowców, stale aktualizowany indeks firm oraz trendy rynkowe."/>
@stop
@section('content')

    <div class="wrapper">
        <div class="page-header section-dark" style="background-image: url('../images/top-bg.jpg')">

            <div class="content-center">
                <div class="container">
                    <div class="title-brand">
                        <h1 class="presentation-title">Twoja-Ekipa</h1>
                        <div class="type">.pl</div>
                        <div class="fog-low">
                            <img src="/img/sections/fog-low.png" alt="">
                        </div>
                        <div class="fog-low right">
                            <img src="/img/sections/fog-low.png" alt="">
                        </div>
                    </div>
                    <br>
                    <br>
                    <p class="presentation-subtitle text-center">Serwis Twoja Ekipa to szeroki wybór fachowców, stale aktualizowany indeks firm oraz trendów rynkowych.
                        Jeżeli poszukujesz fachowej ekipy to jesteś w dobrych rękach.</p>
                </div>

            </div>
        </div>
        <div class="content-center" style="    position: relative;
    z-index: 9;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 ml-auto mr-auto">
                        <h3 style="    font-weight: 400;margin-bottom: 0;" class="title text-dark">Wyszukiwarka wykonawców :</h3>
                    </div>
                    <div class="col-md-12 ml-auto mr-auto">
                        <div class="card card-raised card-form-horizontal no-transition">
                            <div class="card-body">
                                <form method="POST" class="search_form" action="">
                                    <div class="row justify-content-center">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" value="" autocomplete="off" name="state"
                                                       placeholder="Województwo" class="form-control drop-inpt" style="    font-weight: 400;">
                                                <ul class="hidden_drop dropdown-menu dropdown-info"></ul>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <input type="text" value="" autocomplete="off" name="city"
                                                       placeholder="Miasto" class="form-control drop-inpt" style="    font-weight: 400;">
                                                <ul class="hidden_drop dropdown-menu dropdown-info"></ul>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <input type="number" value="" name="refval" placeholder="Ocena [1-5]" min="1" max="5" style="    font-weight: 400;"
                                                       class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <input type="text" name="category" value=""
                                                       placeholder="W zakresie usług" class="form-control drop-inpt" style="    font-weight: 400;">
                                                <ul class="hidden_drop dropdown-menu dropdown-info"></ul>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <button type="submit" class="btn btn-danger btn-block"><i
                                                        class="nc-icon nc-zoom-split"></i> <span style="    font-weight: 400;">Znajdź</span></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="features-2">
            <div class="container">

                <div class="row">
                    <div class="col-md-4">
                        <div class="card" data-background="image"
                             style="background-image: url('/img/sections/jan-sendereks.jpg')">
                            <div class="card-body">
                                <h6 class="card-category">Buduj z Nami</h6>
                                <div class="card-icon">
                                    <i class="fa fa-home"></i>
                                </div>
                                <p class="card-description">Nie marnuj czasu na poszukiwania.

                                    Znajdź odpowiedniego fachowca już dziś!</p>
                                <div class="card-footer">
                                    <a href="/fachowcy" class="btn btn-link btn-neutral">
                                        <i class="fa fa-book" aria-hidden="true"></i> Lista fachowców
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card" data-background="image"
                             style="background-image: url('/img/sections/anders-jilden.jpg')">
                            <div class="card-body">
                                <h6 class="card-category">Daj Lajka</h6>
                                <div class="card-icon">
                                    <i class="fa fa-thumbs-o-up"></i>
                                </div>
                                <p class="card-description">Polub nasz fanpage na facebook’u i

                                    śledź nasz profil aby pozostać na bieżąco.</p>

                                <div class="card-footer">
                                    <a href="https://www.facebook.com/TwojaEkipa/" class="btn btn-link btn-neutral">
                                        <i class="fa fa-book" aria-hidden="true"></i> Fanpage
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card" data-background="image"
                             style="background-image: url('/img/sections/uriel-soberanes.jpg')">
                            <div class="card-body">
                                <h6 class="card-category">Zostaw Po Sobie Ślad</h6>
                                <div class="card-icon">
                                    <i class="fa fa-star"></i>
                                </div>
                                <p class="card-description">Korzystałeś z usług ? Oceń nasz serwis

                                    oraz fachowców wystawiając recenzje..</p>
                                <div class="card-footer">
                                    <a href="/rejestracja" class="btn btn-link btn-neutral">
                                        <i class="fa fa-book" aria-hidden="true"></i> Oceń wykonawcę
                                    </a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="testimonials-2 section section-testimonials">
            <div class="container">
                <div class="row">
                    <div class="col-md-2 mr-auto">
                        <div class="testimonials-people">
                            <img class="left-first-person add-animation"
                                 src="https://s3.amazonaws.com/creativetim_bucket/photos/110862/thumb.?1482812727"
                                 alt="">
                            <img class="left-second-person add-animation"
                                 src="https://s3.amazonaws.com/creativetim_bucket/photos/139481/thumb.jpg?1485460613"
                                 alt="">
                            <img class="left-third-person add-animation"
                                 src="https://s3.amazonaws.com/creativetim_bucket/photos/152441/thumb.jpg?1488233314"
                                 alt="">
                            <img class="left-fourth-person add-animation"
                                 src="https://s3.amazonaws.com/creativetim_bucket/photos/134607/thumb.?1487680276"
                                 alt="">
                            <img class="left-fifth-person add-animation"
                                 src="https://s3.amazonaws.com/creativetim_bucket/photos/161506/thumb.?1489848178"
                                 alt="">
                            <img class="left-sixth-person add-animation"
                                 src="https://s3.amazonaws.com/creativetim_bucket/photos/77627/thumb.jpg?1487360092"
                                 alt="">
                        </div>
                    </div>

                    <div class="col-md-6 ml-auto mr-auto">
                        <div class="page-carousel">
                            <div id="carouselExampleIndicators2" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators2" data-slide-to="0" class="active"></li>
                                    <li data-target="#carouselExampleIndicators2" data-slide-to="1" class=""></li>
                                    <li data-target="#carouselExampleIndicators2" data-slide-to="2" class=""></li>
                                    <li data-target="#carouselExampleIndicators2" data-slide-to="3" class=""></li>
                                </ol>
                                <div class="carousel-inner" role="listbox">

                                    <div class="carousel-item active">
                                        <div class="card card-testimonial card-plain">
                                            <div class="card-avatar">
                                                <img class="img"
                                                     src="https://s3.amazonaws.com/creativetim_bucket/photos/134607/thumb.?1487680276">
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-description">
                                                    Dzięki serwisowi znalazłem odpowiedniego fachowca do remontu domu.
                                                </h5>
                                                <div class="card-footer">
                                                    <h4 class="card-title">Robert K</h4>
                                                    <h6 class="card-category">Zadowolony klient</h6>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="card card-testimonial card-plain">
                                            <div class="card-avatar">
                                                <img class="img" src="images/1.jpg">
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-description">
                                                    Długo szukałam takiego serwisu który pomoże mi w budowie domu.
                                                </h5>
                                                <div class="card-footer">
                                                    <h4 class="card-title">Sylwia T.</h4>
                                                    <h6 class="card-category">Zadowolona klientka</h6>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="card card-testimonial card-plain">
                                            <div class="card-avatar">
                                                <img class="img" src="images/2.jpg">
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-description">
                                                    Serwis godny polecenia!
                                                </h5>
                                                <div class="card-footer">
                                                    <h4 class="card-title">Katarzyna S.</h4>
                                                    <h6 class="card-category">Zadowolona klientka</h6>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="carousel-item">
                                        <div class="card card-testimonial card-plain">
                                            <div class="card-avatar">
                                                <img class="img" src="images/3.jpg">
                                            </div>
                                            <div class="card-body">
                                                <h5 class="card-description">
                                                    Przyjemni klienci, łatwy kontakt !
                                                </h5>
                                                <div class="card-footer">
                                                    <h4 class="card-title">Adam W.</h4>
                                                    <h6 class="card-category">Zadowolony wykonawca</h6>

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <a class="left carousel-control carousel-control-prev"
                                   href="#carouselExampleIndicators2" role="button" data-slide="prev">
                                    <span class="fa fa-angle-left"></span>
                                    <span class="sr-only">Poprzedni</span>
                                </a>
                                <a class="right carousel-control carousel-control-next"
                                   href="#carouselExampleIndicators2" role="button" data-slide="next">
                                    <span class="fa fa-angle-right"></span>
                                    <span class="sr-only">Następny</span>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2 ml-auto">
                        <div class="testimonials-people">
                            <img class="right-first-person add-animation"
                                 src="https://s3.amazonaws.com/uifaces/faces/twitter/mlane/128.jpg" alt="">
                            <img class="right-second-person add-animation"
                                 src="https://s3.amazonaws.com/creativetim_bucket/photos/125268/thumb.jpeg?1497799215"
                                 alt="">
                            <img class="right-third-person add-animation"
                                 src="https://s3.amazonaws.com/creativetim_bucket/photos/123334/thumb.JPG?1479459618"
                                 alt="">
                            <img class="right-fourth-person add-animation"
                                 src="https://s3.amazonaws.com/creativetim_bucket/photos/118235/thumb.?1477435947"
                                 alt="">
                            <img class="right-fifth-person add-animation"
                                 src="https://s3.amazonaws.com/creativetim_bucket/photos/18/thumb.png?1431433244"
                                 alt="">
                            <img class="right-sixth-person add-animation"
                                 src="https://s3.amazonaws.com/creativetim_bucket/photos/167683/thumb.?1491014996"
                                 alt="">
                        </div>
                    </div>

                </div>

            </div>

        </div>
        <div class="team-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto text-center">
                        <h2 class="title">POPULARNI W TYM MIESIĄCU</h2>
                        <h5 class="description">Znajdź swojego wykonawcę, który cieszy się powodzeniem.</h5>
                    </div>
                </div>
                <div class="row mt-3">
                    @each('partials.populars', $populars, 'popular')

                </div>
            </div>
        </div>

    </div>

@stop


@section('page_scripts')
    <script>
        $(function () {

        })
    </script>
@stop