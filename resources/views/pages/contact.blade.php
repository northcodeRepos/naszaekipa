@extends('welcome')

@section('bdcl') contact-us @stop
@section('nvcl') navbar super-nav navbar-expand-lg fixed-top nav-down @stop
@section('seo')
    <title>Twoja Ekipa - Kontakt</title>
    <meta name="description"
          content="Serwis Twoja Ekipa przynosi szeroki wybór fachowców, stale aktualizowany indeks firm oraz trendy rynkowe. Jeżeli poszukujesz fachowej ekipy to jesteś w dobrych rękach."/>
    <meta property="og:title" content="Twoja Ekipa - Jeżeli poszukujesz fachowej ekipy to jesteś w dobrych rękach."/>
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="https://twoja-ekipa.pl/kontakt"/>
    <meta property="og:image" content="https://twoja-ekipa.pl/images/fb.jpg"/>
    <meta property="og:description"
          content="Serwis Twoja Ekipa przynosi szeroki wybór fachowców, stale aktualizowany indeks firm oraz trendy rynkowe."/>
@stop
@section('content')
    <div class="wrapper">
        <div class="section ">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 ml-auto mr-auto text-center">
                        <h2 class="title">Skontaktuj się z nami</h2>
                        <img src="{{asset('images/logo.png')}}" width="150" alt="">
                        <h3><span><i class="fa fa-phone"></i><b> +48 515 716 250</b></span></h3>
                        <h4 style="    margin-top: 10px;"><a class="dark" href="mailto:info@twoja-ekipa.pl"><i
                                        class="fa fa-envelope"></i> info@twoja-ekipa.pl</a></h4>
                        <br><br>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 ml-auto mr-auto text-center">

                        <form class="contact" method="POST" id="contactform" action="">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="name" placeholder="Imię" required>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="surname" placeholder="Nazwisko"
                                           required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="email" class="form-control" name="email" placeholder="Email" required>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="subject" placeholder="Temat" required>
                                </div>
                            </div>
                            <textarea class="form-control" placeholder="Wiadomość" name="message" rows="7"
                                      required></textarea>

                            <div class="row">
                                <div class="col-md-6 ml-auto mr-auto">
                                    <button type="submit" class="btn btn-primary btn-block btn-round">Wyślij</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row">

                    <div class="col-md-12 ml-auto mr-auto text-center">
                        <hr>
                        <h3 class="title">
                            <small>Znajdź nas na social media</small>
                        </h3>
                        <a target="_blank" href="https://www.facebook.com/TwojaEkipa/"
                           class="btn btn-just-icon btn-facebook">
                            <i class="fa fa-facebook"></i>
                        </a>
                        <a target="_blank" href="https://www.instagram.com/twoja_ekipa/"
                           class="btn btn-just-icon btn-instagram">
                            <i class="fa fa-instagram"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('page_scripts')
    <script>
        $(function () {
            $('#contactform').on('submit', function () {
                $('#contactform button').html('Proszę czekać.');
                var data = $(this).serializeArray();
                $.ajax({
                    type: 'post',
                    url: '{{route('contactsend')}}',
                    dataType: 'json',
                    data: {
                        data: data
                    },
                    success: function (data) {
                        $('#contactform button').html('Wyślij');
                        swal(
                            '',
                            '' + data['success'] + '',
                            'success'
                        )
                    },
                    error: function (data) {
                        $('#contactform button').html('Wyślij');
                        swal(
                            '',
                            '' + data['error'] + '',
                            'error'
                        )
                    }
                });
                return false;
            });
        })
    </script>
@stop