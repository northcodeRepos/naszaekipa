@extends('welcome')

@section('bdcl') full-screen register @stop
@section('nvcl') navbar navbar-expand-lg super-nav fixed-top nav-down navbar-transparent @stop
@section('seo')
    <title>Twoja Ekipa - Rejestracja</title>
    <meta name="description"
          content="Serwis Twoja Ekipa przynosi szeroki wybór fachowców, stale aktualizowany indeks firm oraz trendy rynkowe. Jeżeli poszukujesz fachowej ekipy to jesteś w dobrych rękach."/>
    <meta property="og:title" content="Twoja Ekipa - Jeżeli poszukujesz fachowej ekipy to jesteś w dobrych rękach."/>
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="https://twoja-ekipa.pl/rejestracja"/>
    <meta property="og:image" content="https://twoja-ekipa.pl/images/fb.jpg"/>
    <meta property="og:description"
          content="Serwis Twoja Ekipa przynosi szeroki wybór fachowców, stale aktualizowany indeks firm oraz trendy rynkowe."/>
@stop
@section('content')
    <div class="wrapper">
        <div class="page-header" style="background-image: url('../assets/img/sections/soroush-karimi.jpg');">
            <div class="filter"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-7 col-md-6 col-sm-12 col-12 ml-auto">
                        <div class="row">
                            <div class="info info-horizontal">
                                <div class="icon">
                                    <i class="fa fa-home"></i>
                                </div>
                                <div class="description">
                                    <h3> Buduj z Nami </h3>
                                    <p>Nie marnuj czasu na poszukiwania.
                                        Znajdź odpowiedniego fachowca już dziś! <br>
                                        <a href="/fachowcy" class="btn btn-link btn-neutral">
                                            <i class="fa fa-book" aria-hidden="true"></i> Lista fachowców
                                        </a>
                                    </p>
                                </div>

                            </div>
                            <div class="info info-horizontal">
                                <div class="icon">
                                    <i class="fa fa-thumbs-o-up"></i>
                                </div>
                                <div class="description">
                                    <h3>Daj Lajka</h3>
                                    <p>Polub nasz fanpage na facebook’u i
                                        śledź nasz profil aby pozostać na bieżąco. <br>
                                        <a href="https://www.facebook.com/TwojaEkipa/" class="btn btn-link btn-neutral">
                                            <i class="fa fa-book" aria-hidden="true"></i> Fanpage
                                        </a>
                                    </p>
                                </div>
                            </div>
                            <div class="info info-horizontal ">
                                <div class="icon">
                                    <i class="fa fa-star white"></i>
                                </div>
                                <div class="description">
                                    <h3>Zostaw Po Sobie Ślad</h3>
                                    <p>Korzystałeś z usług ? Oceń nasz serwis
                                        oraz fachowców wystawiając recenzje.. <br></p>
                                    <a href="/fachowcy" class="btn btn-link btn-neutral">
                                        <i class="fa fa-book" aria-hidden="true"></i> Oceń wykonawcę
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-6 col-sm-12 col-12 mr-auto">
                        <div class="card card-registercol-12 col-sm-12 col-xs-12 col-lg-12 col-md-12">
                            <div class="container" style="margin-bottom: 0;margin-top: 0;    padding-right: 5px;
    padding-left: 5px;">
                                <h3 style="margin-bottom: 0;" class="card-title col-xs-12"><b><span
                                                class="text-danger text-uppercase">Zarejestruj się !</span></b></h3>
                                <h6 class="card-title col-xs-12"><b>Zajmie ci to mniej niż 2 minuty !</b></h6>
                            </div>
                            <br>
                            <form class="register-form col-12 col-sm-12 col-xs-12 col-lg-12 col-md-12" action="">
                                <div class="row">
                                    <div class="col-md-12 col-lg-12 col-xs-12"><input type="text" name="name"
                                                                                      class="form-control"
                                                                                      placeholder="Imię i Nazwisko"
                                                                                      required></div>
                                    <div class="col-md-12 col-lg-12 col-xs-12"><input type="email" name="email"
                                                                                      class="form-control"
                                                                                      placeholder="Email" required>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-xs-12"><span style="position: relative;"><input
                                                    type="text" autocomplete="off" name="city"
                                                    class="form-control drop-inpt" placeholder="Miasto" required><ul
                                                    class="hidden_drop dropdown-menu dropdown-info"></ul></span></div>


                                    <div class="col-md-12 col-lg-12 col-xs-12"><input type="password" name="password"
                                                                                      class="form-control"
                                                                                      placeholder="Hasło" required
                                                                                      min="5"></div>
                                    <div class="col-md-12 col-lg-12 col-xs-12"><input type="password" name="repassword"
                                                                                      class="form-control"
                                                                                      placeholder="Powtórz hasło"
                                                                                      required min="5"></div>
                                    <div class="col-md-12 col-lg-12 col-xs-12">&nbsp;</div>
                                    <div class="col-md-12 col-lg-12 col-xs-12">
                                        <div class="form-check-radio">
                                            <label class="form-check-label text-uppercase" style="font-weight: bold;">
                                                <input type="radio" checked class="form-check-input" value="1"
                                                       name="isperformer" id="isperformer">
                                                Jestem wykonawcą
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-xs-12">
                                        <div class="form-check-radio">
                                            <label class="form-check-label text-uppercase" style="font-weight: bold;">
                                                <input type="radio" checked class="form-check-input" value="0"
                                                       name="isperformer" id="isperformer2">
                                                Jestem klientem
                                                <span class="form-check-sign"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-xs-12">
                                        <div class="form-check">
                                            <input style="    left: 25px;visibility: visible;" class="form-check-input" type="checkbox" id="rules" name="rules" required>
                                            <span class="form-check-sign" style="position: absolute;"></span>
                                            <label class="form-check-label" for="rules">
                                                Oświadczam, iż zapoznałem się z treścią <a href="/Regulamin.pdf" target="_blank"><b>Regulaminu</b></a> i akceptuję jego
                                                wszystkie postanowienia. Akceptuję <a href="/polityka%20prywatnosci.pdf#" target="_blank"><b>Politykę Prywatności</b></a>.
                                            </label>
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-lg-12 col-xs-12">&nbsp;</div>

                                    <button class="btn btn-block btn-lg">Zarejestruj</button>
                                </div>
                            </form>
                            <div class="login">
                                <p>Masz już konto? <a class="text-danger" href="{{route('logowanie')}}"><b>Zaloguj
                                            się</b></a>.</p>
                            </div>
                            <div class="col-md-12 col-lg-12 col-xs-12">&nbsp;</div>
                        </div>
                    </div>


                </div>
            </div>

        </div>
    </div>
@stop

@section('page_scripts')
    <script>
        $(function () {
            $('.register-form').on('submit', function () {
                $(this).find('button').html('Proszę czekać');
                $.ajax({
                    type: 'post',
                    url: '{{route('registerpost')}}',
                    dataType: 'json',
                    data: {
                        name: $(this).find('input[name=name]').val(),
                        email: $(this).find('input[name=email]').val(),
                        password: $(this).find('input[name=password]').val(),
                        repassword: $(this).find('input[name=repassword]').val(),
                        isperformer: $(this).find('input[name=isperformer]:checked').val(),
                        city: $(this).find('input[name=city]').val(),
                        country: $(this).find('input[name=country]').val(),
                        zipcode: $(this).find('input[name=zipcode]').val()
                    },
                    success: function (data) {
                        $(this).find('button').html('Zarejestruj');
                        if (data['error']) {
                            swal(
                                '',
                                '' + data['error'] + '',
                                'error'
                            )
                        } else {
                            swal(
                                '',
                                '' + data['success'] + '',
                                'success'
                            )
                            window.location.replace("{{route('index')}}");
                        }

                    },
                    error: function (data) {
                        $(this).find('button').html('Zarejestruj');
                        swal(
                            '',
                            'Sprawdź czy pola są wypełnione poprawnie.',
                            'error'
                        )
                    }
                });
                return false;
            });
        })
    </script>
@stop