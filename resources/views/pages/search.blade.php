@extends('welcome')

@section('bdcl') off-canvas-menu @stop
@section('nvcl') navbar navbar-expand-lg fixed-top nav-up super-nav navbar-transparent @stop
@section('seo')
    <title>Twoja Ekipa - Wyniki wyszukiwania</title>
    <meta name="description" content="Serwis Twoja Ekipa przynosi szeroki wybór fachowców, stale aktualizowany indeks firm oraz trendy rynkowe. Jeżeli poszukujesz fachowej ekipy to jesteś w dobrych rękach." />
    <meta property="og:title" content="Twoja Ekipa - Jeżeli poszukujesz fachowej ekipy to jesteś w dobrych rękach."/>
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="{{Request::url()}}"/>
    <meta property="og:image" content="https://twoja-ekipa.pl/images/fb.jpg"/>
    <meta property="og:description" content="Serwis Twoja Ekipa przynosi szeroki wybór fachowców, stale aktualizowany indeks firm oraz trendy rynkowe."/>
@stop
@section('content')
    <div class="section section-gray" id="cards">
        <div class="container tim-container">
            <div class="title" style="margin-top: 110px;">
                <h2 class="title">Wyniki wyszukiwania</h2>
                <h5 class="description">Znaleźliśmy {{$users->count()}} fachowców<br>
                    <br></h5>
            </div>
@if($users->count() == 0) <h3><a href="/">Wróć do strony głównej</a></h3> @endif
            <div class="row cards-gal">
                @if($users)
                    @foreach($users as $user)
                        <div class="col-md-4 col-lg-3 p-2 grid-item">
                            <div class="card col-md-12">
                                <div class="card-body text-center">
                                    @if($user->user_fb)
                                        <a href="{{$user->user_fb}}" class="category-social text-success pull-right">
                                            <i class="fa fa-facebook-square"></i>
                                        </a>
                                    @endif
                                    @if($user->user_tw)
                                        <a href="{{$user->user_tw}}" class="category-social text-info pull-right">
                                            <i class="fa fa-twitter-square"></i>
                                        </a>
                                    @endif
                                    @if($user->user_url)
                                        <a href="{{$user->user_url}}" class="category-social text-dark pull-right">
                                            <i class="fa fa-external-link" style="font-size: 20px; margin-top: 0;"></i>
                                        </a>
                                    @endif
                                    <div class="clearfix"></div>
                                    <div class="author">
                                        <a href="{{route('profile', $user->id)}}" target="_blank">
                                            <img src="@if($user->getFirstMediaUrl('avatars')) {{$user->getFirstMediaUrl('avatars')}} @else /avatars/default-avatar.png  @endif" class="avatar-big img-raised border-gray">

                                            <p class="card-category" style="font-size: 10px;"><span class="text-info"
                                                                                                    style="font-weight: 800;">@if($user->user_isperformer)
                                                        Miasto {{$user->user_city}} @else Klient
                                                    @endif</span> @if($user->user_avg == 0) Brak
                                                ocen @else <br> <b>Śr. ocena
                                                    : </b><?php $i = 1; while($i <= $user->user_avg) {
                                                ?><i class="fa fa-star"></i><?php $i++; } ?> @endif</p>
                                            <h5 class="card-title">{{$user->display_name}}</h5>

                                            @if($user->categories)
                                                <p style="    font-size: 11px;" class="text-danger category">
                                                    <?php $c = 1; ?>
                                                    @foreach($user->categories as $cat)
                                                        @if($c <= 5)
                                                            @<?php echo $cat->name?>
                                                        @endif
                                                        <?php $c++; ?>
                                                    @endforeach
                                                </p>
                                            @endif </a>
                                    </div>
                                    @if($user->user_desc)
                                        <p class="card-description">
                                            {{\Illuminate\Support\Str::limit($user->user_desc, 50)}}...
                                        </p>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
            @if($users)
            <div class="row">
                <div class="col-12 text-center">
                    <br><br><br>
                    <div class="d-inline-block">{{ $users->links() }}</div>
                </div>
            </div>
            @endif
        </div>
    </div>

@stop


@section('page_scripts')
    <script>
        $(function () {

        })
    </script>
@stop