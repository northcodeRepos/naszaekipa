<!doctype html>
<html lang="{{ app()->getLocale() }}" class="js cssanimations">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">


    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport'/>
    <meta name="viewport" content="width=device-width"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @yield('seo')

<!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="//www.googletagmanager.com/gtag/js?id=UA-109153707-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-109153707-1');
    </script>

    @include('partials.style')
</head>
<body class="presentation-page @yield('bdcl')">
<nav class="@yield('nvcl')" color-on-scroll="50">
    <div class="container">
        <div class="navbar-translate">
            <button class="navbar-toggler navbar-toggler-right navbar-burger" type="button" data-toggle="collapse"
                    data-target="#navbarToggler" aria-controls="navbarTogglerDemo02" aria-expanded="false"
                    aria-label="Toggle navigation">
                <span class="navbar-toggler-bar"></span>
                <span class="navbar-toggler-bar"></span>
                <span class="navbar-toggler-bar"></span>
            </button>
            <a class="navbar-brand" href="/"><img src="{{asset('images/logo.png')}}" width="100" alt=""></a>
        </div>
        <div class="collapse navbar-collapse" id="navbarToggler">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item hidden-md">
                    <a class="nav-link" rel="tooltip" data-placement="bottom"
                       href="https://www.facebook.com/TwojaEkipa/" target="_blank">
                        <i class="fa fa-facebook-square"></i>
                        <p class="d-lg-none">Facebook</p>
                    </a>
                </li>
                <li class="nav-item  hidden-md">
                    <a class="nav-link" rel="tooltip" data-placement="bottom"
                       href="https://www.instagram.com/twoja_ekipa/" target="_blank">
                        <i class="fa fa-instagram"></i>
                        <p class="d-lg-none">Instagram</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="/" class="nav-link"><i class="nc-icon nc-air-baloon"></i> Strona Główna</a>
                </li>
                {{--<li class="nav-item">--}}
                    {{--<a href="/blog" target="_blank" class="nav-link"><i class="nc-icon nc-book-bookmark"></i>--}}
                        {{--Blog</a></li>--}}
                <li class="nav-item">
                    <a href="/fachowcy" class="nav-link"><i class="nc-icon nc-user-run"></i>
                        Fachowcy</a></li>
                <li class="nav-item">
                    <a href="/kontakt" class="nav-link"><i class="nc-icon nc-bulb-63"></i>
                        Kontakt</a></li>
                @if(!Auth::check())
                    <li class="nav-item">
                        <a href="/logowanie" class="btn btn-danger btn-round">Logowanie</a></li>
                    <li class="nav-item">
                        <a href="/rejestracja" class="btn btn-danger btn-round">Rejestracja</a></li>
                @else
                    <li class="nav-item" style="position: relative;">
                        <button href="#" class=" dropdown-toggle btn btn-round btn-info btn-block" data-toggle="dropdown">Witaj {{strstr(Auth::user()->display_name, ' ', true)}} </button>
                        <!-- You can add classes for different colours on the next element -->
                        <ul class="dropdown-menu dropdown-info">
                            <a class="dropdown-item" href="/profil/{{Auth::user()->id}}">Twój Profil</a>
                            <a class="dropdown-item" href="/edytuj/{{Auth::user()->id}}">Edytuj Profil</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="/wyloguj">Wyloguj się</a>
                        </ul>
                    </li>
                @endif

            </ul>
        </div>
    </div>
</nav>
@yield('content')
@include('partials.footer')
{{--<footer class="footer footer-black">--}}
    {{--<div class="container">--}}
        {{--<div class="row">--}}
            {{--<nav class="footer-nav">--}}
                {{--<ul>--}}
                    {{--<li><a href="/blog">Blog</a></li>--}}
                    {{--<li><a target="_blank" href="{{asset('pomoc.pdf')}}">Pomoc</a></li>--}}
                    {{--<li><a target="_blank" href="{{asset('pliki cookie.pdf')}}">Pliki Cookie</a></li>--}}
                    {{--<li><a target="_blank" href="{{asset('polityka-prywatnosci.pdf')}}">Polityka Prywatności</a></li>--}}
                {{--</ul>--}}
            {{--</nav>--}}
            {{--<div class="credits ml-auto">--}}
				{{--<span class="copyright">--}}
					{{--© 2017 Twoja-Ekipa <i class="fa fa-heart heart"></i> Wszelkie Prawa Zastrzeżone - <a href="https://gowebsite.pl" target="_blank">Powered By Gowebsite</a>--}}
				{{--</span>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</footer>--}}
@include('partials.scripts')

@yield('page_scripts')
</body>
</html>