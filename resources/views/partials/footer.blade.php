<footer class="footer footer-black footer-big">
    <div class="container">
        <div class="row">
            <div class="col-md-2 text-center col-sm-3 col-12 ml-auto mr-auto">
                <img style="    margin-top: 20px;
    width: 120px;" src="{{asset('images/logo.png')}}" alt="">
            </div>
            <div class="col-md-9 col-sm-9 col-12 ml-auto mr-auto">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-6">
                        <div class="links">
                            <ul class="uppercase-links stacked-links">
                                <li>
                                    <a href="{{route('index')}}">
                                        Strona Główna
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('agents')}}">
                                        Fachowcy
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('contact')}}">
                                        Kontakt
                                    </a>
                                </li>

                            </ul>

                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-6">
                        <div class="links">
                            <ul class="uppercase-links stacked-links">
                                <li>
                                    <a href="{{route('rejestracja')}}">
                                        Zarejestruj się
                                    </a>
                                </li>
                                <li>
                                    <a href="{{route('logowanie')}}">
                                        Zaloguj się
                                    </a>
                                </li>
                                @if(!Auth::check())
                                    <li>
                                        <a href="{{route('logowanie')}}">Twój profil</a>
                                    </li>
                                @else
                                    <li>
                                        <a href="profil/{{Auth::user()->id}}">Twój profil</a>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-6">
                        <div class="links">
                            <ul class="uppercase-links stacked-links">
                                <li>
                                    <a target="_blank" href="{{asset('pomoc.pdf')}}">pomoc</a>
                                </li>
                                <li>
                                    <a target="_blank" href="{{asset('pliki cookie.pdf')}}">Pliki Cookie</a>
                                </li>
                                <li>
                                    <a target="_blank" href="{{asset('polityka-prywatnosci.pdf')}}">Polityka Prywatności</a>
                                </li>

                            </ul>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-6">
                        <div class="links">
                            <ul class="stacked-links">
                                <li>
                                    <h4>{{$counter}}<br> <small>wykonawców</small></h4>
                                </li>
                                <li>
                                    <h4>{{$counter2}}<br> <small>klientów</small></h4>
                                </li>

                            </ul>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="copyright">
                    <div class="pull-left text-left text-danger">
                        © <script>document.write(new Date().getFullYear())</script> Twoja-Ekipa <i class="fa fa-heart heart"></i> Wszelkie Prawa Zastrzeżone
                        <br><a style="color: #ffffff !important;font-weight: 500;font-size: 14px;" href="https://gowebsite.pl" target="_blank" class="text-danger">projekt / wykonanie strony : Gowebsite.pl</a>
                    </div>
                    <div class="links pull-right">
                        <ul>
                            <li>
                                <a target="_blank" href="{{asset('pomoc.pdf')}}">Pomoc</a>
                            </li>
                            -
                            <li>
                                <a target="_blank" href="{{asset('pliki cookie.pdf')}}">Pliki Cookie</a>
                            </li>
                            -
                            <li>
                                <a target="_blank" href="{{asset('polityka-prywatnosci.pdf')}}">Polityka Prywatności</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    </div>
</footer>