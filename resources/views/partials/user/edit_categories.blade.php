@if($user->user_isperformer == 1)
    <form id="edit_categories" method="POST" action="{{route('edit_cats')}}"
          class="text-left col-xs-12 edit_form d-flex flex-wrap flex-column justify-content-start">
        <div class="row mb-2">
            <div class="col-md-12">
                <h3>Kategorie :</h3>
            </div>
        </div>
        <div class="row" id="cats">
            @if($user->categories)
                <?php $c = 1 ?>
                @foreach($user->categories as $cat)
                    <div class="col-md-6 position-relative mt-2">
                        <div class="row">
                            <div class="col-md-1 col-1 col-sm-1 d-flex p-0 justify-content-center align-items-center">
                                <i class="fa fa-id-card"></i></div>
                            <div class="col-md-11 col-11 col-sm-11 pl-0">
                                <ul class="hidden_drop dropdown-menu dropdown-info"></ul>
                                <input type="text" name="category{{$c}}"
                                       value="{{$cat->name}}" placeholder="Kategoria {{$c}}"
                                       class="form-control drop-inpt mb-0" autocomplete="off" required>
                                <span class="remove_cat btn btn-sm btn-danger"><i
                                            class="fa fa-trash"></i> Usuń</span>
                            </div>
                        </div>
                    </div>
                    <?php $c++ ?>
                @endforeach
        </div>
        <div class="row mt-5">
            <div class="col-md-12">
                <button class="btn btn-success" type="submit"><i class="fa fa-save"></i> Zapisz
                    kategorie
                </button>
                <span class="addcat btn btn-info"><i
                            class="fa fa-plus"></i> Dodaj kategorię</span>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-md-12">
                <hr>
            </div>
        </div>
        @endif

    </form>
@endif