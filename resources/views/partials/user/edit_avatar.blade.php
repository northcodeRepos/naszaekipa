<form enctype="multipart/form-data" method="POST" action="{{route('change_av')}}"
      class="fileinput fileinput-new text-center flex-column">
    <div class="fileinput-new thumbnail img-circle img-no-padding d-flex">
        @include('partials.components.avatar', ['avatar' => $user->getFirstMediaUrl('avatars')])
    </div>
    <div class="fileinput-preview fileinput-exists thumbnail img-circle img-no-padding mw-150 mh-150"></div>
    <span class="btn btn-default btn-file btn-round">
                                    <span class="fileinput-new">Zmień Zdjęcie</span><input type="file" name="avatar" required>
                                </span>
</form>