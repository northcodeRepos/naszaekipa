@if($user->user_isperformer == 1)
    <div class="row text-left" style="position: relative;">
        <div class="gallery_loader">
            <div class="uil-reload-css reload-background"
                 style="    background-color: #414d9b;">
                <div></div>
            </div>
        </div>
        <div class="col-md-12"><h4 class="text-left">Galeria prac użytkownika</h4>
            <p class="text-info">Maksymalny rozmiar jednego zdjęcia to 1,5 MB, większe nie
                zostaną dodane.
                <br> Dozwolone są tylko pliki obrazkowe. <br></p><br></div>


        <div class="col-md-12 col-xs-12">
            <form id="gallery_add" style="width: 100%;" enctype="multipart/form-data"
                  method="POST" action="{{route('gallery_add')}}">
                <label for="add_file" style="cursor: pointer;"
                       class="card grid-item">
                    <span class="add-photo btn btn-info"><i class="fa fa-plus"></i> Dodaj zdjęcia</span></span>
                    <input style="opacity: 0;position: absolute;" id="add_file"
                           accept="image/jpeg,image/jpg,image/gif" type="file"
                           name="gallery_add[]" multiple>
                </label>
            </form>
        </div>

        <div class="col-md-12">
            <div id="gallery" class="col-md-12" style="padding: 0;">
                @foreach(Auth::user()->getMedia('portfolio')->reverse() as $media)
                    <div class="card grid-item "
                         style="overflow:hidden;padding: 0;width: 31%;margin: 1%;">
                        <img src="{{$media->getUrl()}}" alt="">
                        <span media_id="{{$media->id}}"
                              class="deletephoto label label-danger">USUŃ ZDJĘCIE</span>
                    </div>
                @endforeach
            </div>
        </div>
        @if(Auth::user()->getMedia('portfolio')->count() > 10)
            <div class="col-md-12 col-xs-12">&nbsp;</div>
            <div class="col-md-12 col-xs-12">&nbsp;</div>
            <div class="col-md-12 col-xs-12">
                <form id="gallery_add" style="width: 100%;" enctype="multipart/form-data"
                      method="POST" action="{{route('gallery_add')}}">
                    <label for="add_file" style="cursor: pointer;"
                           class="card grid-item">
                        <span class="add-photo btn btn-info"><i class="fa fa-plus"></i> Dodaj zdjęcia</span></span>
                        <input style="opacity: 0;position: absolute;" id="add_file"
                               accept="image/jpeg,image/jpg,image/gif" type="file"
                               name="gallery_add[]" multiple>
                    </label>
                </form>
            </div>

            <!-- Tab panes -->

        @endif
    </div>
@endif
