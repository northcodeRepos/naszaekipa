<form enctype="multipart/form-data"
      method="POST"
      action="{{route('crop_av')}}"
      class="crop_av" style="display: none;">

    <input type="hidden" name="x" value=""/>
    <input type="hidden" name="y" value=""/>
    <input type="hidden" name="xx" value=""/>
    <input type="hidden" name="yy" value=""/>

    <h3 class="mt-0 mb-1">Przytnij avatar</h3>
    <p class="mb-2">Zaznacz obszar</p>
    <div class="form-group">
        <div class="row">
            <img src="" alt="">
        </div>
    </div>
    <div class="form-group">
        <div class="row justify-content-center">
            <button class="btn-success btn" type="submit">Zapisz zdjęcie</button>
        </div>
    </div>
</form>