<form id="edit_details" method="POST" action="{{route('edit_details')}}"
      class="text-left edit_form">
    <div class="row">
        <h3 class="col-12">Dane użytkownika :</h3>
    </div>
    <div class="row mt-3">
        <div class="col-md-6 position-relative mt-2">
            <div class="row">
                <div class="col-md-1 col-1 col-sm-1 d-flex p-0 justify-content-center align-items-center">
                    <i class="fa fa-envelope"></i></div>
                <div class="col-md-11 col-11 col-sm-11 pl-0">
                    <input class="form-control input-sm mb-0" type="email" disabled
                           value="{{$user->email}}">
                </div>
            </div>
        </div>
        <div class="col-md-6 position-relative mt-2">
            <div class="row">
                <div class="col-md-1 col-1 col-sm-1 d-flex p-0 justify-content-center align-items-center">
                    <i class="fa fa-address-card-o"></i></div>
                <div class="col-md-11 col-11 col-sm-11 pl-0">
                    <ul class="hidden_drop dropdown-menu dropdown-info"></ul>
                    <input class="form-control input-sm mb-0  drop-inpt" type="text"
                           placeholder="Miasto" name="city" autocomplete="off"
                           value="{{$user->user_city}}" required>
                </div>
            </div>
        </div>
        <div class="col-md-6 position-relative mt-2">
            <div class="row">
                <div class="col-md-1 col-1 col-sm-1 d-flex p-0 justify-content-center align-items-center">
                    <i class="fa fa-address-card-o"></i></div>
                <div class="col-md-11 col-11 col-sm-11 pl-0">
                    <input class="form-control input-sm mb-0" type="text"
                           autocomplete="off" name="zipcode" placeholder="Kod Pocztowy (00-000)"
                           value="{{$user->user_zipcode}}" required>
                </div>
            </div>
        </div>
        <div class="col-md-6 position-relative mt-2">
            <div class="row">
                <div class="col-md-1 col-1 col-sm-1 d-flex p-0 justify-content-center align-items-center">
                    <i class="fa fa-globe"></i></div>
                <div class="col-md-11 col-11 col-sm-11 pl-0">
                    <ul class="hidden_drop dropdown-menu dropdown-info"></ul>
                    <input type="text" value="{{$user->user_state}}" autocomplete="off"
                           name="state"
                           placeholder="Pomorskie" class="form-control drop-inpt mb-0">
                </div>
            </div>
        </div>
        <div class="col-md-6 position-relative mt-2">
            <div class="row">
                <div class="col-md-1 col-1 col-sm-1 d-flex p-0 justify-content-center align-items-center">
                    <i class="fa fa-address-card-o"></i></div>
                <div class="col-md-11 col-11 col-sm-11 pl-0">
                    <input class="form-control input-sm mb-0"
                           placeholder="ul. Przykładowa 12a/2"
                           type="text" name="address"
                           value="{{$user->user_address}}">
                </div>
            </div>
        </div>
        <div class="col-md-6 position-relative mt-2">
            <div class="row">
                <div class="col-md-1 col-1 col-sm-1 d-flex p-0 justify-content-center align-items-center">
                    <i class="fa fa-globe"></i></div>
                <div class="col-md-11 col-11 col-sm-11 pl-0">
                    <input class="form-control input-sm mb-0"
                           type="text" name="country" placeholder="Polska"
                           value="{{$user->user_country}}">
                </div>
            </div>
        </div>
        <div class="col-md-6 position-relative mt-2">
            <div class="row">
                <div class="col-md-1 col-1 col-sm-1 d-flex p-0 justify-content-center align-items-center">
                    <i class="fa fa-phone"></i></div>
                <div class="col-md-11 col-11 col-sm-11 pl-0">
                    <input class="form-control input-sm mb-0"
                           placeholder="+48 123-456-789"
                           type="tel" name="number"
                           value="{{$user->user_number}}">
                </div>
            </div>
        </div>
        <div class="col-md-6 position-relative mt-2">
            <div class="row">
                <div class="col-md-1 col-1 col-sm-1 d-flex p-0 justify-content-center align-items-center">
                    <i class="fa fa-external-link"></i></div>
                <div class="col-md-11 col-11 col-sm-11 pl-0">
                    <input placeholder="Strona internetowa"
                           class="form-control input-sm mb-0"
                           type="text" name="url"
                           value="{{$user->user_url}}">
                </div>
            </div>
        </div>
        @if($user->user_isperformer == 1)
            <div class="col-md-12">
                <div class="row">
                    <hr>
                </div>
            </div>
            <div class="col-md-6 position-relative mt-2">
                <div class="row">
                    <div class="col-md-1 col-1 col-sm-1 d-flex p-0 justify-content-center align-items-center">
                        <i class="fa fa-facebook"></i></div>
                    <div class="col-md-11 col-11 col-sm-11 pl-0">
                        <input placeholder="Facebook"
                               class="form-control input-sm mb-0"
                               type="text" name="fb"
                               value="{{$user->user_fb}}">
                    </div>
                </div>
            </div>
            <div class="col-md-6 position-relative mt-2">
                <div class="row">
                    <div class="col-md-1 col-1 col-sm-1 d-flex p-0 justify-content-center align-items-center">
                        <i class="fa fa-twitter"></i></div>
                    <div class="col-md-11 col-11 col-sm-11 pl-0">
                        <input placeholder="Twitter"
                               class="form-control input-sm mb-0"
                               type="text" name="tw"
                               value="{{$user->user_tw}}">
                    </div>
                </div>
            </div>
            <div class="col-md-6 position-relative mt-2">
                <div class="row">
                    <div class="col-md-1 col-1 col-sm-1 d-flex p-0 justify-content-center align-items-center">
                        <i class="fa fa-hashtag"></i></div>
                    <div class="col-md-11 col-11 col-sm-11 pl-0">
                        <input class="form-control input-sm mb-0"
                               placeholder="Numer Regon"
                               type="number" name="regon"
                               value="{{$user->user_regon}}">
                    </div>
                </div>
            </div>
            <div class="col-md-6 position-relative mt-2">
                <div class="row">
                    <div class="col-md-1 col-1 col-sm-1 d-flex p-0 justify-content-center align-items-center">
                        <i class="fa fa-hashtag"></i></div>
                    <div class="col-md-11 col-11 col-sm-11 pl-0">
                        <input class="form-control input-sm mb-0"
                               placeholder="Numer Nip"
                               type="number" name="nip"
                               value="{{$user->user_nip}}">
                    </div>
                </div>
            </div>
            <div class="col-md-6 position-relative mt-2">
                <div class="row">
                    <div class="col-md-1 col-1 col-sm-1 d-flex p-0 justify-content-center align-items-center">
                        <i class="fa fa-copyright"></i></div>
                    <div class="col-md-11 col-11 col-sm-11 pl-0">
                        <input class="form-control input-sm mb-0"
                               placeholder="Nazwa firmy"
                               type="text" name="company_name"
                               value="{{$user->company_name}}">
                    </div>
                </div>
            </div>
        @endif
    </div>

    <div class="row mt-5">
        <div class="col-md-12">
            <button class="btn btn-success"
                    type="submit"><i
                        class="fa fa-save"></i> Zapisz profil</button>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-md-12">
            <hr>
        </div>
    </div>
</form>