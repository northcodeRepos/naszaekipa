<form id="edit_desc" method="POST" action="{{route('edit_desc')}}"
      class="text-left edit_form">
    <div class="row">
        <h3 class="col-md-12">Opis użytkownika :</h3>
    </div>
    <div class="form-group mt-3">
        <textarea class="form-control textarea-limited" name="message" placeholder="Opis użytkownika." rows="10" ,="" maxlength="1000">{{$user->user_desc}}</textarea>
    </div>
    <div class="row mt-4">
        <div class="col-md-12">
            <button class="btn btn-success" type="submit"><i class="fa fa-save"></i> Zapisz opis</button>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-md-12">
            <hr>
        </div>
    </div>
</form>