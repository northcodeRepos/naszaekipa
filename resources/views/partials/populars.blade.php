<div class="col-md-3">
    <div class="card card-profile card-plain">
        <div class="card-avatar">
            <a href="{{route('profile', $popular->id)}}"> @include('partials.components.avatar', ['avatar' => $popular->getFirstMediaUrl('avatars')])</a>

        </div>
        <div class="card-body">
            <a href="{{route('profile', $popular->id)}}">
                <div class="author">
                    <h4 class="card-title">{{$popular->user_nicename}}</h4>
                    <h6 class="card-category text-muted">@if($popular->user_isperformer)
                            Wykonawca @else Klient @endif</h6>
                    <small class="">
                        <?php $c = 1; ?>
                        @foreach($popular->categories as $cat)
                            @if($c <= 5)
                                {{$cat->name}} @if($cat != $popular->categories->last())
                                    , @endif
                            @endif
                            <?php $c++; ?>
                        @endforeach
                    </small>
                </div>
            </a>
            @if($popular->user_desc)
                <p class="card-description text-center">
                    {{\Illuminate\Support\Str::limit($popular->user_desc, 55)}}
                </p>
            @endif
            <h6 class="card-category text-muted">Średnia ocena
                : @if($popular->user_avg == 0) Brak
                ocen @else {{$popular->user_avg}}
                <i class="fa fa-star"></i>@endif </h6>
            <h6 class="card-category text-muted">Opinii : @if($popular->opinions_count == 0)
                    Brak opinii @else {{$popular->opinions_count}} <i
                            class="fa fa-comment"></i>@endif</h6>
        </div>
    </div>
</div>
