<link href='//fonts.googleapis.com/css?family=Montserrat:400,300,700' rel='stylesheet' type='text/css'>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
<link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet" />
<link href="{{asset('css/paper-kit.css')}}" rel="stylesheet"/>
<link href="{{asset('css/demo.css')}}" rel="stylesheet" />
<link href="{{asset('css/nucleo-icons.css')}}" rel="stylesheet">
<link href="{{asset('css/lightbox.min.css')}}" rel="stylesheet">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/cropper/3.1.3/cropper.min.css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.1/sweetalert2.min.css">
<style type="text/css">
    :root [class^="ad-"]
    { display: none !important; }</style>