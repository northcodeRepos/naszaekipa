<script src="{{asset('js/jquery-3.2.1.min.JS')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery-ui-1.12.1.custom.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/popper.js')}}" type="text/javascript"></script>
<script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>

<!-- Switches -->
<script src="{{asset('js/bootstrap-switch.min.js')}}"></script>

<!--  Plugins for Slider -->
<script src="{{asset('js/nouislider.js')}}"></script>

<!--  Plugins for DateTimePicker -->
<script src="{{asset('js/moment.min.js')}}"></script>
<script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>

<!--  Paper Kit Initialization and functons -->
<script src="{{asset('js/paper-kit.js?v=2.1.0')}}"></script>
<script src="//unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
<script src="{{asset('js/main.js')}}"></script>
<script src="{{asset('js/buttons.js')}}"></script>
<script src="{{asset('js/bootstrap-select.js')}}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.1/sweetalert2.min.js"></script>
<script src='//www.google.com/recaptcha/api.js'></script>
<script src="{{asset('js/demo.js ')}}"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/cropper/3.1.3/cropper.min.js"></script>
<script type="text/javascript">
    (function () {
        function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        }
    })();
</script>
<script>
    $(function () {
        $(window).scrollTop($(window).scrollTop() - 1);
        $(window).scrollTop($(window).scrollTop() + 1);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

</script>

<script src="{{asset('js/optimalization.js')}}"></script>
<script src="{{asset('js/pl.js')}}"></script>
<script>
    $(function () {
        $('.deletephoto').on('click', function () {
            var id = $(this).attr('media_id');
            $.get('/deletephoto/' + id, function (data) {
                if (data == 'true') {
                    $('span[media_id=' + id + ']').parent().remove();
                    $('#gallery').masonry('destroy');
                    $('#gallery').masonry({
                        // options
                        itemSelector: '.grid-item',
                    })
                }
            });
        });

        $('.search_form').on('submit', function () {
            $('.search_form button[type=submit]').html('Proszę czekać');
            $.ajax({
                type: 'post',
                url: '{{route('search_form')}}',
                dataType: 'json',
                data: $(this).serialize(),
                success: function (data) {
                    if (data['error']) {
                        swal(
                            '',
                            '' + data['error'] + '',
                            'error'
                        )
                    } else {
                        window.location.replace(data['url']);
                    }
                },
                error: function (data) {
                    swal(
                        '',
                        'Sprawdź czy pola są wypełnione poprawnie.',
                        'error'
                    )
                }
            });

            return false;
        });

        $('.cards-gal').masonry({
            // options
            itemSelector: '.grid-item',
        });
        $('#gallery').masonry({
            // options
            itemSelector: '.grid-item'
        });



        var submitGalleryAdd = function (e) {
            var formData = new FormData(this);
            $('.gallery_loader').toggleClass('active');
            $.ajax({
                type: 'POST',
                url: "{{route('gallery_add')}}",
                data: formData,
                processData: false,
                contentType: false,
                success: function (data) {
                    $('.gallery_loader').toggleClass('active');
                    data = JSON.parse(data);
                    if (data['success']) {
                        swal(
                            '',
                            '' + data['success'] + '',
                            'success'
                        )
                    }
                    if (data['error']) {
                        swal(
                            '',
                            '' + data['error'] + '',
                            'error'
                        )
                    } else {

                    }
                }
            });
            return false;
        };
        $('#gallery_add input').on('change', function () {
            $('#gallery_add').submit()
        });
        $('#gallery_add').on('submit', submitGalleryAdd);
        $('#service_date').datetimepicker({
            locale: 'pl',
            format: 'MMMM'
        });
        $('.stars_check .card-stars i').on('mouseenter', function () {
            $(this).parent().find('i').removeClass('active');
            var index = $(this).index();
            var this_index = 1;
            for (this_index = 1; this_index < index + 2; this_index++) {
                $(this).parent().find('i:nth-child(' + this_index + ')').addClass('active');
            }
            ;
        });
        $('.stars_check .card-stars i').on('click', function () {
            var rate = $(this).index() + 1;
            $(this).parent().parent().find('input').val(rate);
        });

        var addopinion = function (e) {
            var formData = new FormData(this);
            $.ajax({
                type: 'POST',
                url: "{{route('addopinion')}}",
                data: formData,
                processData: false,
                contentType: false,
                success: function (data) {
                    data = JSON.parse(data);
                    if (data.errors) {
                        $('#add_opinion').find('label').find('small.has-error').remove();
                        $('#add_opinion').find('input.form-control').removeClass('has-error');
                        $.each(data.errors, function (key, value) {
                            $('#add_opinion *[name=' + key + ']').addClass('has-error');
                            $('#add_opinion *[name=' + key + ']').parent().find('label:first-of-type').append('<small class="has-error"><small class="control-label"> ' + value + '</small></small>');
                        });

                        $('html, body').animate({
                            scrollTop: $('.has-error').offset().top - 300
                        }, 100);
                    }
                    if (data.error) {
                        swal(
                            '',
                            '' + data.error + '',
                            'error'
                        )
                    }
                    if (data.success) {
                        swal(
                            '',
                            '' + data.success + '',
                            'success'
                        )
                    }
                }
            });
            return false;
        };

        $('#add_opinion textarea[name=message]').on('keyup', function () {
            $(this).parent().find('label:last-of-type small').html('Pozostało: ' + (1000 - $(this).val().length) + '/1000 znaków.');
            if ((1000 - $(this).val().length) < 1) $(this).parent().find('label:last-of-type').removeClass('text-info').addClass('text-danger');
            if ($(this).val().length < 50) $('#add_opinion button[type=submit]').attr('disabled', 'disabled');
            if ((1000 - $(this).val().length) > 1) $(this).parent().find('label:last-of-type').removeClass('text-danger').addClass('text-info');
            if ($(this).val().length > 50) $('#add_opinion button[type=submit]').removeAttr('disabled');
        });
        $('#add_opinion').on('submit', addopinion);
    });
    $(document).ready(function () {

        setTimeout(function () {
            if ($('#gallery').length) {
                $('#gallery').masonry({
                    // options
                    itemSelector: '.grid-item'
                });
            }
        }, 500);
    });
</script>
<script src="{{asset('js/lightbox.min.js')}}"></script>
