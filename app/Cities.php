<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cities extends Model
{
    /**
     * @var string
     */
    protected $postType = 'wp_cities';
}
