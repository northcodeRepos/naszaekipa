<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use App\Notifications\AuthResetPasswordNotification;

class User extends Authenticatable implements HasMedia {

    use HasMediaTrait;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new AuthResetPasswordNotification($token));
    }
    public function categories()
    {
        $cats = new userCategories();
        return $this->hasMany($cats)->whereHas('category')->with('category')->join('categories', 'user_categories.category_id', '=', 'categories.id');
    }

    public function followers()
    {
        return $this->hasMany(Fav::class,'fav_id')->join('users', 'favs.user_id', '=', 'users.id');
    }

    public function follows()
    {
        return $this->hasMany(Fav::class,'user_id')->join('users', 'favs.fav_id', '=', 'users.id');
    }

    public function opinions()
    {
        return $this->hasMany(Opinion::class, 'op_id');
    }

    public function getUserByID($id)
    {
        $user = DB::table('users')->where('users.id', $id)->first();
        $user_categories = userCategories::where('user_id', $user->id)->join('categories', 'user_categories.category_id', '=', 'categories.id')->get();
        $user_categories = collect($user_categories)->pluck('name')->toArray();
        $user = collect($user);
        $user['user_categories'] = $user_categories;

        return $user->except(['user_pass', 'user_activation_key', 'user_status']);
    }
}
