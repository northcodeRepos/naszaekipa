<?php

namespace App\Http\Controllers;

use App\Categories;
use App\Fav;
use App\Opinion;
use App\User;
use App\userCategories;
use Carbon\Carbon;
use DeepCopy\f008\A;
use function GuzzleHttp\Psr7\str;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Validator;
use Intervention\Image\Image;
use View;
use Mail;
use Illuminate\Support\Facades\Redirect;
use function MongoDB\BSON\toJSON;
use Psr\Http\Message\ServerRequestInterface;

class HomeController extends Controller
{
    public function __construct()
    {
        //its just a dummy data object.
        $counter = User::where('user_isperformer', 1)->count();
        $counter2 = User::where('user_isperformer', 0)->count();
        // Sharing is caring
        View::share('counter', $counter);
        View::share('counter2', $counter2);
    }

    public function email_poster(Request $request) {
      $file = fopen(public_path() . "/emails.txt", "w") or die("Unable to open file!");
      $txt = $request->get('emails');
      fwrite($file, "\n". $txt);
      fclose($file);

      return json_encode(['success' => $request->get('emails')]);
    }

    public function deletePhoto($id)
    {

        if (!Auth::user()->getMedia('portfolio')->where('id', $id)) return json_encode(['error' => 'Wybierz zdjęcie które chcesz usunąć.']);
        $photo = Auth::user()->getMedia('portfolio')->where('id', $id)->first()->delete();
        return json_encode(true);
    }

    public function gallery_add(Request $request)
    {
        if (!$request->hasFile('gallery_add')) return json_encode(['error' => 'Wybierz zdjęcie/zdjęcia.']);
        $urls = collect([]);
        foreach (collect($request->allFiles('gallery_add'))->get('gallery_add') as $file) {

            if (Auth::user()->getMedia('portfolio')->count() > 30) return json_encode(['error' => 'Możesz dodać maksymalnie 20 zdjęć do galerii.']);
            if (substr($file->getMimeType(), 0, 5) === 'image') {
                $img = \Intervention\Image\Facades\Image::make($file);
                if ($img->filesize() < 1500000) {
                    if ($img->width() > 1920) {
                        $img->resize(1920, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                    }
                    $randomName = str_random() . '.' . $file->getClientOriginalExtension();
                    $path = public_path('/temp/' . $randomName);
                    $img->save($path);
                    $new = Auth::user()->addMedia($path)->toMediaCollection('portfolio');
                    if (file_exists($path)) unlink($path);
                }
            }
        }


        return json_encode(['success' => 'Zdjęcia zostaly dodane, będą widoczne po odświeżeniu.']);

    }

    public function change_av(Request $request)
    {

        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $check = \Intervention\Image\Facades\Image::make($avatar);
            if ($check->filesize() > 1000000) return json_encode(['error' => 'Zdjęcie nie może przekraczać 1mb.']);
            $check->resize(320, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $check->save(public_path('/avatars/temp_' . Auth::user()->id . '.jpg'));

            return json_encode(['src' => 'temp_' . Auth::user()->id . '.jpg']);
        }
    }

    public function crop_av(Request $request)
    {

        $x = round($request->get('x'));
        $xx = round($request->get('xx'));
        $y = round($request->get('y'));
        $yy = round($request->get('yy'));
        if (file_exists(public_path('avatars/temp_' . Auth::user()->id . '.jpg'))) {

            $img = \Intervention\Image\Facades\Image::make(public_path('avatars/temp_' . Auth::user()->id . '.jpg'));
            $img->crop($xx, $yy, $x, $y);
            $img->save(public_path('avatars/' . Auth::user()->id . '.jpg'));

            $user = Auth::user();
            $user->clearMediaCollection('avatars');
            $user->addMedia(public_path('avatars/' . Auth::user()->id . '.jpg'))->toMediaCollection('avatars');


            return json_encode(['success' => 'Zapisano.', 'src' => Auth::user()->getFirstMediaUrl('avatars')]);
        }
        return json_encode(['error' => 'Błąd.']);
    }

    public function index()
    {
        $populars = User::whereHas('opinions')
            ->with('opinions')
            ->withCount('opinions')
            ->where('user_avg', '>=', 4)
            ->limit(8)
            ->get();

        return view('pages.home', compact('populars'));
    }

    public function contact()
    {
        return view('pages.contact');
    }

    public function search_form(Request $request)
    {
        $data = $request->all();
        $region = 'brak';
        $city = 'brak';
        $refval = 0;
        $category = 0;
        $data = collect($data);
        if ($data->get('state')) $region = $data->get('state');
        if ($data->get('city')) $city = $data->get('city');
        if ($data->get('refval')) $refval = $data->get('refval');
        if ($data->get('category')) $category = (Categories::where('name', $data->get('category'))->first())->id;

        return json_encode(['url' => route('search', [$region, $city, $refval, $category])]);
    }

    public function search($region, $city, $refval, $category)
    {

        $q = User::query();
        if ($region != 'brak') $q->where('user_state', $region);
        if ($city != 'brak') $q->where('user_city', $city);
        if ($category != 0) $q->whereHas('categories', function ($query) use ($category) {
            $query->join('categories', 'user_categories.category_id', '=', 'categories.id')->where('categories.id', $category);

        });
        if ($refval != 0) $q->where('user_avg', $refval);

        $users = $q->with('categories')->where('user_isperformer', 1)->withCount('opinions')->orderBy('user_avg', 'DESC')->paginate(20);
        return view('pages.search', compact('users', 'refval'));
    }

    public function agents()
    {

        if (Auth::check()) {
            $city = Auth::user()->user_city;
            $users = User::withCount('opinions')
                ->where('user_isperformer', 1)
                ->with('categories')
                ->orderByRaw("FIELD(user_city , '$city') DESC")
                ->orderBy('user_avg', 'DESC')
                ->paginate(20);
        } else {
            $users = User::withCount('opinions')->where('user_isperformer', 1)->with('categories')->orderBy('user_avg', 'DESC')->paginate(20);
        }
        return view('pages.agents', compact('users'));
    }

    public function edytuj($id)
    {

    }

    public function profile($id)
    {
        $user = User::where('id', $id)
            ->with('categories')
            ->with('opinions')
            ->with('followers')
            ->with('follows')
            ->first();

        return view('pages.profile', compact('user'));
    }

    public function edit($id)
    {
        if (Auth::user()->id != $id) return redirect(route('index'));
        $user = User::where('id', $id)
            ->with('categories')
            ->with('opinions')
            ->with('followers')
            ->with('follows')
            ->first();
        return view('pages.edit', compact('user'));
    }

    public function fav($id)
    {
        if (!$id) return Redirect::route('index');
        $fav_id = User::where('id', $id)->first();
        if (!$fav_id) return Redirect::route('index');
        if (!Auth::check()) return Redirect::route('profile', $id);
        $check = Fav::where('user_id', Auth::user()->id)->where('fav_id', $fav_id->id)->count();
        if ($check != 0) return Redirect::route('profile', $id);

        $newFav = new Fav();
        $newFav->user_id = Auth::user()->id;
        $newFav->fav_id = $fav_id->id;
        $newFav->created_at = Carbon::now();
        $newFav->save();

        if ($newFav) return Redirect::route('profile', $id);

    }

    public function edit_details(Request $request)
    {

        if (!Auth::check()) return json_encode(['error' => 'Coś poszło nie tak, upewnij się, że poprawnie wypełniłeś/łaś formularz.']);
        $data = collect($request->all());

        $user = $request->user();
        $user->user_city = $data->get('city');
        $user->user_zipcode = $data->get('zipcode');
        $user->user_country = $data->get('country');
        $user->user_fb = $data->get('fb');
        $user->user_tw = $data->get('tw');
        $user->user_regon = $data->get('regon');
        $user->user_nip = $data->get('nip');
        $user->company_name = $data->get('company_name');
        $user->user_url = $data->get('url');
        $user->user_state = $data->get('state');
        $user->user_number = $data->get('number');
        $user->user_address = $data->get('address');
        $user->save();

        return json_encode(['success' => 'Zapisano']);

    }

    public function edit_desc(Request $request)
    {

        if (!Auth::check()) return json_encode(['error' => 'Coś poszło nie tak, upewnij się, że poprawnie wypełniłeś/łaś formularz.']);
        $message = collect($request->all())->get('message');
        $userNow = $request->user();
        $userNow->user_desc = $message;
        $userNow->save();
        return json_encode(['success' => 'Zapisano']);

    }

    public function edit_cats(Request $request)
    {
        if (!Auth::check()) return json_encode(['error' => 'Coś poszło nie tak, upewnij się, że poprawnie wypełniłeś/łaś formularz.']);
        if (collect($request->all())->count() < 1) return json_encode(['error' => 'Musisz posiadać przynajmniej jedną przypisaną kategorię']);
        userCategories::where('user_id', $request->user()->id)->delete();
        foreach (collect($request->all()) as $category) {
            $newCategory = new userCategories();
            $newCategory->user_id = $request->user()->id;
            $newCategory->category_id = (Categories::where('name', $category)->first())->id;
            $newCategory->save();
        }
        return json_encode(['success' => 'Zapisano']);
    }

    public function delcat($id)
    {

    }

    public function delfav($id)
    {
        if (!$id) return Redirect::route('index');
        $fav_id = User::where('id', $id)->first();
        if (!$fav_id) return Redirect::route('index');
        if (!Auth::check()) return Redirect::route('profile', $id);
        $check = Fav::where('user_id', Auth::user()->id)->where('fav_id', $fav_id->id)->count();
        if ($check != 1) return Redirect::route('profile', $id);

        $fav = Fav::where('user_id', Auth::user()->id)->where('fav_id', $id)->delete();
        return Redirect::route('profile', $id);

    }

    public function login()
    {
        if (!Auth::check()) return view('pages.login');
        return redirect(route('index'));
    }

    public function logout()
    {
        Auth::logout();
        return redirect(route('index'));
    }


    public function addopinion(Request $request)
    {
        $ref = $request->header()['referer'][0];
        $ref = explode('/profil/', $ref, 2)[1];


        $validator = Validator::make($request->all(), [
            'initial_impression' => 'required|between:1,5|numeric',
            'overall' => 'required|between:1,5|numeric',
            'price' => 'required|between:1,5|numeric',
            'punctuality' => 'required|between:1,5|numeric',
            'quality' => 'required|between:1,5|numeric',
            'message' => 'required|min:50|max:1000',
            'would_recommend' => 'required|boolean',
            'service_date' => 'required',
            'media1' => 'mimes:jpeg,gif,png|max:1000',
            'media2' => 'mimes:jpeg,gif,png|max:1000',
            'media3' => 'mimes:jpeg,gif,png|max:1000'
        ]);
        $errors = new Collection();
        if ($validator->fails()) {
            $errorPack = $validator->errors();

            if ($errorPack->has('initial_impression')) $errors->push(['initial_impression' => 'To pole jest nie poprawne.']);
            if ($errorPack->has('overall')) $errors->push(['overall' => 'To pole jest nie poprawne.']);
            if ($errorPack->has('price')) $errors->push(['price' => 'To pole jest nie poprawne.']);
            if ($errorPack->has('punctuality')) $errors->push(['punctuality' => 'To pole jest nie poprawne.']);
            if ($errorPack->has('quality')) $errors->push(['quality' => 'To pole jest nie poprawne.']);
            if ($errorPack->has('would_recommend')) $errors->push(['would_recommend' => 'To pole jest nie poprawne.']);
            if ($errorPack->has('message')) $errors->push(['message' => 'Pole powinno zawierać od 50 do 1000 znaków.']);
            if ($errorPack->has('service_date')) $errors->push(['service_date' => 'Wybierz datę wykonania usługi.']);
            if ($errorPack->has('media1')) $errors->push(['media1' => 'Zdjęcie jest w złym formacie lub waży więcej niż 1mb.']);
            if ($errorPack->has('media2')) $errors->push(['media2' => 'Zdjęcie jest w złym formacie lub waży więcej niż 1mb.']);
            if ($errorPack->has('media3')) $errors->push(['media3' => 'Zdjęcie jest w złym formacie lub waży więcej niż 1mb.']);

        }
        $errors = $errors->flatMap(function ($values) {
            return array_map(function ($values) {
                return $values;
            }, $values);
        });
        if ($errors->count() > 0) return json_encode(['errors' => $errors]);
        if (!Auth::check() || !$request->user()) return json_encode(['error' => 'Musisz być zalogowany.']);
        if (Opinion::where('user_id', Auth::user()->id)->where('op_id', $ref)->where('op_verified', 0)->count() > 0) return json_encode(['error' => 'Posiadasz oczekującą do sprawdzenia przez moderatora opinię na temat tego użytkownika. Cierpliwości.']);


        $opinion = new Opinion();
        $opinion->user_id = $request->user()->id;
        $opinion->op_id = $ref;
        $opinion->initial_impression = $request->get('initial_impression');
        $opinion->message = $request->get('message');
        $opinion->overall = $request->get('overall');
        $opinion->price = $request->get('price');
        $opinion->punctuality = $request->get('punctuality');
        $opinion->quality = $request->get('quality');
        $opinion->would_recommend = $request->get('would_recommend');
        $opinion->service_date = $request->get('service_date');
        $opinion->op_verified = 0;
        $opinion->op_rate = round(collect($request->all())->except('message', 'would_recommend', 'media1', 'media2', 'media3', 'service_date','cleanliness')->avg());
        $opinion->save();

        if ($opinion && $request->hasFile('media1')) {
            $media = $request->file('media1');
            $img = \Intervention\Image\Facades\Image::make($media);
            if ($img->filesize() < 1000000) {
                if ($img->width() > 1920) {
                    $img->resize(1920, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $randomName = str_random() . '.' . $media->getClientOriginalExtension();
                $path = public_path('/temp/' . $randomName);
                $img->save($path);
                $new = $opinion->addMedia($path)->toMediaCollection('opinion');
                if (file_exists($path)) unlink($path);
            }
        }
        if ($opinion && $request->hasFile('media2')) {
            $media = $request->file('media2');
            $img = \Intervention\Image\Facades\Image::make($media);
            if ($img->filesize() < 1000000) {
                if ($img->width() > 1920) {
                    $img->resize(1920, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $randomName = str_random() . '.' . $media->getClientOriginalExtension();
                $path = public_path('/temp/' . $randomName);
                $img->save($path);
                $new = $opinion->addMedia($path)->toMediaCollection('opinion');
                if (file_exists($path)) unlink($path);
            }
        }
        if ($opinion && $request->hasFile('media3')) {
            $media = $request->file('media3');
            $img = \Intervention\Image\Facades\Image::make($media);
            if ($img->filesize() < 1000000) {
                if ($img->width() > 1920) {
                    $img->resize(1920, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                }
                $randomName = str_random() . '.' . $media->getClientOriginalExtension();
                $path = public_path('/temp/' . $randomName);
                $img->save($path);
                $new = $opinion->addMedia($path)->toMediaCollection('opinion');
                if (file_exists($path)) unlink($path);
            }
        }
        return json_encode(['success' => 'Opinia została przesłana do moderatora.']);

    }

    public function registerpost(Request $request)
    {
        $email = $request->get('email');
        if (!$request->get('name') || !$request->get('email') || !$request->get('password') || !$request->get('repassword')) return json_encode(['error' => 'Wypełnij wszystkie pola']);
        if ($request->get('password') != $request->get('repassword')) return json_encode(['error' => 'Podane hasła nie zgadzają się ze sobą']);
        if (User::where('email', $email)->first()) return json_encode(['error' => 'Podany użytkownik jest już zarejestrowany']);
        $name = $request->get('name');
        $password = $request->get('password');
        $repassword = $request->get('repassword');
        $isperformer = $request->get('isperformer');

        $newUser = new User();
        $newUser->name = strstr($email, '@', true);
        $newUser->password = Hash::make($password);
        $newUser->user_nicename = $name;
        $newUser->email = $email;
        $newUser->user_registered = Carbon::now();
        $newUser->display_name = $name;
        $newUser->user_isperformer = $isperformer;
        $newUser->user_city = $request->get('city');
        $newUser->user_isperformer = $isperformer;
        $newUser->user_avatar = 'default-avatar.png';

        $newUser->save();
        if ($newUser) {
            Auth::login($newUser);
        }
        return json_encode(['success' => 'Zostałeś zalogowany']);
    }

    public function register()
    {
        if (!Auth::check()) return view('pages.registerr');
        return redirect(route('index'));
    }

    public function contactsend(Request $request)
    {
        $details = $request->all();
        $details = collect($details['data']);
        $details = $details->pluck('value');
        Mail::send('emails.contact', ['details' => $details], function ($m) use ($details) {
            $m->from('noreply@twoja-ekipa.pl', 'Twoja Ekipa');
            $m->to('info@twoja-ekipa.pl', $details[0])->subject('Kontakt ze strony Twoja Ekipa!');
            $m->bcc('jacek.j93@gmail.com', $details[0])->subject('Kontakt ze strony Twoja Ekipa!');
        });

        return json_encode(['success' => 'Dziękjemy za kontakt']);

    }

    public function getstate($name)
    {

        $state = DB::table('state')->where('name', 'LIKE', $name . '%')->get(['name']);
        $state = collect($state)->pluck('name');
        return json_encode($state);
    }

    public function getcity($name)
    {
        $state = DB::table('cities')->where('nazwa', 'LIKE', $name . '%')->limit(30)->get(['nazwa']);
        $state = collect($state)->unique('nazwa')->pluck('nazwa');
        return json_encode($state);
    }

    public function getcat($name)
    {
        $state = DB::table('categories')->where('name', 'LIKE', $name . '%')->orWhere('name', 'LIKE', '%' . $name . '%')->limit(15)->get(['name']);
        $state = collect($state)->unique('name')->pluck('name');
        return json_encode($state);
    }
}
