<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class userCategories extends Model
{
    /**
     * @var string
     */
    protected $postType = 'wp_user_categories';

    public function category() {
        return $this->belongsTo(Categories::class,'category_id');
    }

}
