<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class States extends Model
{
    /**
     * @var string
     */
    protected $table = 'wp_state';
}
