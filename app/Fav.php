<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fav extends Model
{
    /**
     * @var string
     */
    protected $postType = 'wp_favs';
}
