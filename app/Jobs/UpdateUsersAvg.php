<?php

namespace App\Jobs;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class UpdateUsersAvg implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->users = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $update = $this->users->whereHas('opinions')
            ->with('opinions')
            ->get();

        foreach ($update as $user) {
            $count = 0;
            foreach ($user->opinions as $opinion) {
                if ($opinion->op_verified == 1) {
                    $count = $count + $opinion->op_rate;
                }
            }
            $rate = round($count / $user->opinions->count());
            $user->user_avg = $rate;
            $user->save();
        }

    }
}
