<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class UpdateUsersAvg extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:avg';
    protected $users;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(User $users)
    {
        parent::__construct();
        $this->users = $users;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $update = $this->users->whereHas('opinions')
            ->with('opinions')
            ->get();

        foreach ($update as $user) {
            $count = 0;
            foreach ($user->opinions as $opinion) {
                if ($opinion->op_verified == 1) {
                    $count = $count + $opinion->op_rate;
                }
            }
            $rate = round($count / $user->opinions->count());
            $user->user_avg = $rate;
            $user->save();
        }
    }
}
