<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Corcel\Corcel;

class RunWordpressSync extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'syncwordpress';
    protected $signature = 'syncwordpress';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'syncwordpress';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(Corcel $corcel)
    {
    }

}
