<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;

class Opinion extends Model implements HasMedia {

    use HasMediaTrait;

    protected $table = 'opinions';
    protected $fillable = [
        'user_id', 'op_id', 'op_verified','cleanliness','initial_impression','message','overall','price','punctuality','quality','service_date'
    ];


}
